﻿using PMS.ApplicationService.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.ApplicationService.ServiceContract
{
    public interface IBRSIncrementNoService : IDisposable
    {
        BRSIncrementNoDto GetBRSNo();
        void UpdateBRSNo();
    }
}
