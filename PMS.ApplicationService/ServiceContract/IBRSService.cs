﻿using PMS.ApplicationService.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.ApplicationService.ServiceContract
{
    public interface IBRSService : IDisposable
    {
        void CreateBRS(BRSDto brs);
        BRSDto getBRSDetails(int id);
        IEnumerable<BRSDto> GetAll();
        void UpdateBRS(BRSDto brs);
        void UpdatePISNo(BRSDto brs);
        void DeleteBRS(int id);
        BRSDto GetPISNo(string pisNo);
    }
}
