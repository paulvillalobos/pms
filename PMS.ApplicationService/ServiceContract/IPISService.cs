﻿using PMS.ApplicationService.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.ApplicationService.ServiceContract
{
    public interface IPISService : IDisposable
    {
        void CreatePIS(PISDto pis);
        PISDto getPISDetails(Guid PkId, int Id);
        IEnumerable<PISDto> GetAll();
        void UpdatePIS(PISDto pis);
        void DeletePIS(int id);
        void UpdateIsApproved(PISDto pis);
        void UpdateIsBRS(PISDto pis);
        void UpdateIsSubmit(int id);
        void UpdatePISRecommendation(PISDto pis);
<<<<<<< HEAD
        void UpdateCabSchedule(PISDto pis);
        void UpdateApprovedPIS(int id);
        void UpdateSendBackPIS(PISDto pis);
        IEnumerable<PISDto> getListStatus(string listname);
        //PISDto getListStatus(string listname);
=======


>>>>>>> e0c77c851bfb9fa3f68ef227cf7bc87ed6aca07c
    }
}
