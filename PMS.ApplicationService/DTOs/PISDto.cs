﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.ApplicationService.DTOs
{
    public class PISDto
    {
        DateTime dateTime2 = DateTime.MinValue;
        public int Id { get; set; }
        public string PISNo { get; set; }
        public string Name { get; set; }
        public string DepartmentBU { get; set; }
        public string Position { get; set; }
        public string Manager { get; set; }
        public string BusinessObjective { get; set; }
        public string Request { get; set; }
        public string SolutionRecommendation { get; set; }
        public DateTime? DateReceived { get; set; }
        public string ReceivedBy { get; set; }
        public DateTime? DateViewed { get; set; } 
        public string ReviewedBy { get; set; }
        public string RequestStatus { get; set; }
        public string ProjectWeight { get; set; }
        public string AppDevTeam { get; set; }
        public string AppManager { get; set; }
        public string Title { get; set; }
        public string AppName { get; set; }
        public string AppType { get; set; }
        public string AppRequestor { get; set; }
        public string AppDevComment { get; set; }
        public string AppDevNote { get; set; }
        public DateTime? CabReviewSchedule { get; set; } 
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public Nullable<bool> IsBRS { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateCreated { get; set; } 
        public string EditedBy { get; set; }
        public DateTime? DateEdited { get; set; }
        public Nullable<bool> Deleted { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DateDeleted { get; set; }
        public Nullable<bool> IsDraft { get; set; }
        public Nullable<bool> IsSubmit { get; set; }
        public Nullable<bool> IsApproved { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? DateApproved { get; set; }
        public System.Guid PkId { get; set; }
        public string AppDev { get; set; }
<<<<<<< HEAD
        public Nullable<bool> IsApprovedPIS { get; set; }
        public Nullable<System.DateTime> DateApprovedPIS { get; set; }
        public string ApprovedPISBy { get; set; }
        public Nullable<bool> IsSendBack { get; set; }
        public Nullable<System.DateTime> DateSendBack { get; set; }
        public string SendBackBy { get; set; }

        public string CabNote { get; set; }
        public string SendBackNote { get; set; }


        public string BRSNo { get; set; }
        public int BRSId { get; set; }
=======
>>>>>>> e0c77c851bfb9fa3f68ef227cf7bc87ed6aca07c
    }
}
