﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.ApplicationService.DTOs
{
    public class PISDevDto
    {
        public int Id { get; set; }
        public string PISNo { get; set; }
        public string DevName { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public string EditedBy { get; set; }
        public DateTime DateEdited { get; set; }
        public bool Deleted { get; set; }
        public string DeletedBy { get; set; }
        public DateTime DateDeleted { get; set; }
    }
}
