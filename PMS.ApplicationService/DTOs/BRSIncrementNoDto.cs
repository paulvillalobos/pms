﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.ApplicationService.DTOs
{
    public class BRSIncrementNoDto
    {
        public int Id { get; set; }
        public int BRSNo { get; set; }
        public string BRSNodisplay { get; set; }
    }
}
