﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.ApplicationService.DTOs
{
    public class PISIncrementNoDto
    {
        public int Id { get; set; }
        public int PISNo { get; set; }
        public string PISNodisplay { get; set; }
    }
}
