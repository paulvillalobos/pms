﻿using AutoMapper;
using PMS.ApplicationService.DTOs;
using PMS.ApplicationService.ServiceContract;
using PMS.Common.Logger;
using PMS.Domain.Contracts;
using PMS.Domain.Entities;
using PMS.Infrastructure;
using PMS.Infrastructure.EntityFramework.PMS;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PMS.ApplicationService.Services
{
    public class PISIncrementNoService : IPISIncrementNoService
    {
        private readonly IPISIncrementNo _pISIncrementNoReposotory;
        private IPMSUnitOfWork _pMSUnitOfWork;
        private readonly IMapper _mapper;
        private static readonly ILogCentral logCentral = LogCentral.GetLogger(typeof(PISIncrementNoService));

        public PISIncrementNoService(IPISIncrementNo pISIncrementNoReposotory,
            IPMSUnitOfWork pMSUnitOfWork,
            IMapper mapper)
        {
            if (pISIncrementNoReposotory == null) throw new ArgumentNullException("PISIncrementNoService");
            if (pMSUnitOfWork == null) throw new ArgumentNullException("UnitOfWork");
            if (mapper == null) throw new ArgumentNullException("Mapper");
            _pISIncrementNoReposotory = pISIncrementNoReposotory;
            _pMSUnitOfWork = pMSUnitOfWork;
            _mapper = mapper;
        }

        public PISIncrementNoDto GetPISNo()
        {
            try
            {
                var brsList = _pISIncrementNoReposotory.GetPISNo();
                return _mapper.Map<PISEIncrementNo, PISIncrementNoDto>(brsList);
            }
            catch (Exception ex)
            {
                logCentral.Error("GetBRSNo", ex);
                throw;
            }
        }

        public void UpdatePISNo()
        {
            try
            {
                _pISIncrementNoReposotory.UpdatePISNum();
            }
            catch (Exception ex)
            {
                logCentral.Error("Update BRSNo", ex);
                throw;
            }
        }

        public void Dispose()
        {
            _pMSUnitOfWork.Dispose();
        }
    }
}
