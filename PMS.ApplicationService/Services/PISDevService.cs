﻿using AutoMapper;
using PMS.ApplicationService.DTOs;
using PMS.ApplicationService.ServiceContract;
using PMS.Common.Logger;
using PMS.Domain.Contracts;
using PMS.Domain.Entities;
using PMS.Infrastructure;
using PMS.Infrastructure.EntityFramework.PMS;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PMS.ApplicationService.Services
{
    public class PISDevService : IPISDevService
    {
        private readonly IPISDevRepository _pISDevRepository;
        private IPMSUnitOfWork _pMSUnitOfWork;
        private readonly IMapper _mapper;
        private static readonly ILogCentral logCentral = LogCentral.GetLogger(typeof(PISDevService));

        public PISDevService(IPISDevRepository pISDevRepository,
            IPMSUnitOfWork pMSUnitOfWork,
            IMapper mapper)
        {
            if (pISDevRepository == null) throw new ArgumentNullException("PISDevService");
            if (pMSUnitOfWork == null) throw new ArgumentNullException("UnitOfWork");
            if (mapper == null) throw new ArgumentNullException("Mapper");
            _pISDevRepository = pISDevRepository;
            _pMSUnitOfWork = pMSUnitOfWork;
            _mapper = mapper;
        }

        public void Dispose()
        {
            _pMSUnitOfWork.Dispose();
        }
    }
}
