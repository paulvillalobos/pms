﻿using AutoMapper;
using PMS.ApplicationService.DTOs;
using PMS.ApplicationService.ServiceContract;
using PMS.Common.Logger;
using PMS.Domain.Contracts;
using PMS.Domain.Entities;
using PMS.Infrastructure;
using PMS.Infrastructure.EntityFramework.PMS;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PMS.ApplicationService.Services
{
    public class BRSIncrementNoService : IBRSIncrementNoService
    {
        private readonly IBRSIncrementNo _bRSIncrementNoRepository;
        private IPMSUnitOfWork _pMSUnitOfWork;
        private readonly IMapper _mapper;
        private static readonly ILogCentral logCentral = LogCentral.GetLogger(typeof(BRSIncrementNoService));

        public BRSIncrementNoService(IBRSIncrementNo bRSIncrementNoRepository,
            IPMSUnitOfWork pMSUnitOfWork,
            IMapper mapper)
        {
            if (bRSIncrementNoRepository == null) throw new ArgumentNullException("BRSIncrementNoService");
            if (pMSUnitOfWork == null) throw new ArgumentNullException("UnitOfWork");
            if (mapper == null) throw new ArgumentNullException("Mapper");
            _bRSIncrementNoRepository = bRSIncrementNoRepository;
            _pMSUnitOfWork = pMSUnitOfWork;
            _mapper = mapper;
        }


        public BRSIncrementNoDto GetBRSNo()
        {
            try
            {
                var brsList = _bRSIncrementNoRepository.GetBRSNo();
                return _mapper.Map<BRSEIncrementNo, BRSIncrementNoDto>(brsList);
            }
            catch (Exception ex)
            {
                logCentral.Error("GetBRSNo", ex);
                throw;
            }
        }

        public void UpdateBRSNo()
        {
            try
            {
                _bRSIncrementNoRepository.UpdateBRSNum();
            }
            catch (Exception ex)
            {
                logCentral.Error("Update BRSNo", ex);
                throw;
            }
        }

        public void Dispose()
        {
            _pMSUnitOfWork.Dispose();
        }
    }
}
