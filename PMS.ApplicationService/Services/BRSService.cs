﻿using AutoMapper;
using PMS.ApplicationService.DTOs;
using PMS.ApplicationService.ServiceContract;
using PMS.Common.Logger;
using PMS.Common.EmailHelper;
using PMS.Domain.Contracts;
using PMS.Domain.Entities;
using PMS.Infrastructure;
using PMS.Infrastructure.EntityFramework.PMS;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace PMS.ApplicationService.Services
{
    public class BRSService : IBRSService
    {
        private readonly IBRSRepository _bRSRepository;
        private readonly IEmailManager _emailManager;
        private IPMSUnitOfWork _pMSUnitOfWork;
        private readonly IMapper _mapper;
        private static readonly ILogCentral logCentral = LogCentral.GetLogger(typeof(BRSService));

        public BRSService(IBRSRepository bRSRepository, IPMSUnitOfWork pMSUnitOfWork, IMapper mapper, IEmailManager emailManager)
        {
            if (bRSRepository == null) throw new ArgumentNullException("BRSService");
            if (pMSUnitOfWork == null) throw new ArgumentNullException("UnitOfWork");
            if (mapper == null) throw new ArgumentNullException("Mapper");
            if (emailManager == null) throw new ArgumentNullException("EmailManager");
            _bRSRepository = bRSRepository;
            _pMSUnitOfWork = pMSUnitOfWork;
            _mapper = mapper;
            _emailManager = emailManager;
        }

        public void CreateBRS(BRSDto brs)
        {
            try
            {
                BRS newBRS = BRS.Create(_mapper.Map<BRSDto, BRS>(brs));
                if (newBRS != null)
                {
                    _bRSRepository.Add(newBRS);
                    logCentral.Info(string.Format("BRS {0} has been created", newBRS.BRSNo));
                    //For Email testing purposes only - Begin
                    //Dictionary<string, string> emailData = new Dictionary<string, string>();
                    //emailData.Add("{ApproverFName}", employee.FirstName);
                    //emailData.Add("{ApproverLName}", employee.LastName);
                    //emailData.Add("{ApplicationType}", employee.BadgeId);
                    //emailData.Add("{ListPageUrl}", "Google.com");
                    //emailData.Add("{ApplicantFName}", employee.EmployeeId);
                    //emailData.Add("{ApplicantLName}", employee.AccessNo.ToString());
                    //emailData.Add("{EffectiveDate}", "April 1, 2018");
                    //emailData.Add("{EffectiveTimeFrom}", "09:00AM");
                    //emailData.Add("{EffectiveTimeTo}", "07:30PM");
                    //emailData.Add("{DateSubmitted}", DateTime.Now.ToLongDateString());
                    //_emailManager.SendMail(
                    //    new EmailSender(ConfigurationManager.AppSettings["EmailSender"]),
                    //    "pauljohn.villalobos@robinsonsretail.com.ph",
                    //    "This is a test mail!",
                    //    "ApprovalNotification",
                    //    emailData);
                    //For Email testing purposes only - End
                }
                else
                {
                    logCentral.Info("Create BRS: Invalid BRS information");
                }
            }
            catch (Exception ex)
            {
                logCentral.Error("Create BRS", ex);
                throw;
            }
        }


        public BRSDto getBRSDetails(int id)
        {
            try
            {
                var pis = _mapper.Map<BRS, BRSDto>(_bRSRepository.GetById(id));
                return pis;
            }
            catch (Exception ex)
            {
                logCentral.Error("getPISDetails", ex);
                throw;
            }
        }


        public IEnumerable<BRSDto> GetAll()
        {
            try
            {
                var brsList = _bRSRepository.GetAll();
                return _mapper.Map<IEnumerable<BRS>, IEnumerable<BRSDto>>(brsList);
            }
            catch (Exception ex)
            {
                logCentral.Error("GetAll", ex);
                throw;
            }
        }
        
        public BRSDto GetPISNo(string pisNo)
        {
            try
            {
                var brsList = _bRSRepository.GetAll().Where(e => e.PISNo == pisNo).FirstOrDefault();
                return _mapper.Map<BRS, BRSDto>(brsList);
            }
            catch (Exception ex)
            {
                logCentral.Error("GetAll", ex);
                throw;
            }
        }

        public void UpdateBRS(BRSDto brs)
        {
            try
            {
                BRS newBRS = BRS.Update(_mapper.Map<BRSDto, BRS>(brs));
                if (newBRS != null)
                {
                    _bRSRepository.Update(newBRS);
                    logCentral.Info(string.Format("BRS {0} has been updated", newBRS.BRSNo));

                    //Dictionary<string, string> emailData = new Dictionary<string, string>();
                    //emailData.Add("{ApproverFName}", employee.FirstName);
                    //emailData.Add("{ApproverLName}", employee.LastName);
                    //emailData.Add("{ApplicationType}", employee.BadgeId);
                    //emailData.Add("{ListPageUrl}", "Google.com");
                    //emailData.Add("{ApplicantFName}", employee.EmployeeId);
                    //emailData.Add("{ApplicantLName}", employee.AccessNo.ToString());
                    //emailData.Add("{EffectiveDate}", "April 1, 2018");
                    //emailData.Add("{EffectiveTimeFrom}", "09:00AM");
                    //emailData.Add("{EffectiveTimeTo}", "07:30PM");
                    //emailData.Add("{DateSubmitted}", DateTime.Now.ToLongDateString());
                    //_emailManager.SendMail(
                    //    new EmailSender(ConfigurationManager.AppSettings["EmailSender"]),
                    //    "pauljohn.villalobos@robinsonsretail.com.ph",
                    //    "This is a test mail!",
                    //    "ApprovalNotification",
                    //    emailData);

                }
                else
                {
                    logCentral.Info("Update BRS: Invalid BRS information");
                }
            }
            catch (Exception ex)
            {
                logCentral.Error("Update BRS", ex);
                throw;
            }
        }

        public void UpdatePISNo(BRSDto brs)
        {
            try
            {
                BRS newBRS = BRS.Update(_mapper.Map<BRSDto, BRS>(brs));
                if (newBRS != null)
                {
                    _bRSRepository.UpdatePISNo(newBRS);
                    logCentral.Info(string.Format("BRS {0} has been updated", newBRS.PISNo));
                }
                else
                {
                    logCentral.Info("Update BRS: Invalid BRS information");
                }
            }
            catch (Exception ex)
            {
                logCentral.Error("Update BRS", ex);
                throw;
            }
        }

        public void DeleteBRS(int id)
        {
            try
            {
                _bRSRepository.Remove(id);
                logCentral.Info(string.Format("BRS with id = {0} has been deleted", id));

                //Dictionary<string, string> emailData = new Dictionary<string, string>();
                //emailData.Add("{ApproverFName}", employee.FirstName);
                //emailData.Add("{ApproverLName}", employee.LastName);
                //emailData.Add("{ApplicationType}", employee.BadgeId);
                //emailData.Add("{ListPageUrl}", "Google.com");
                //emailData.Add("{ApplicantFName}", employee.EmployeeId);
                //emailData.Add("{ApplicantLName}", employee.AccessNo.ToString());
                //emailData.Add("{EffectiveDate}", "April 1, 2018");
                //emailData.Add("{EffectiveTimeFrom}", "09:00AM");
                //emailData.Add("{EffectiveTimeTo}", "07:30PM");
                //emailData.Add("{DateSubmitted}", DateTime.Now.ToLongDateString());
                //_emailManager.SendMail(
                //    new EmailSender(ConfigurationManager.AppSettings["EmailSender"]),
                //    "pauljohn.villalobos@robinsonsretail.com.ph",
                //    "This is a test mail!",
                //    "ApprovalNotification",
                //    emailData);

            }
            catch (Exception ex)
            {
                logCentral.Error("Remove BRS", ex);
                throw;
            }
        }

        public void Dispose()
        {
            this._pMSUnitOfWork.Dispose();
        }
    }
}
