﻿using AutoMapper;
using PMS.ApplicationService.DTOs;
using PMS.ApplicationService.ServiceContract;
using PMS.Common.Logger;
using PMS.Common.EmailHelper;
using PMS.Domain.Contracts;
using PMS.Domain.Entities;
using PMS.Infrastructure;
using PMS.Infrastructure.EntityFramework.PMS;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace PMS.ApplicationService.Services
{
    public class PISService : IPISService
    {
        private readonly IPISRepository _pISRepository;
        private readonly IEmailManager _emailManager;
        private IPMSUnitOfWork _pMSUnitOfWork;
        private readonly IMapper _mapper;
        private static readonly ILogCentral logCentral = LogCentral.GetLogger(typeof(PISService));

        public PISService(IPISRepository pISRepository,
            IPMSUnitOfWork pMSUnitOfWork,
            IMapper mapper,
            IEmailManager emailManager)
        {
            if (pISRepository == null) throw new ArgumentNullException("PISService");
            if (pMSUnitOfWork == null) throw new ArgumentNullException("PMSUnitOfWork");
            if (mapper == null) throw new ArgumentNullException("Mapper");
            if (emailManager == null) throw new ArgumentNullException("EmailManager");
            _pISRepository = pISRepository;
            _pMSUnitOfWork = pMSUnitOfWork;
            _mapper = mapper;
            _emailManager = emailManager;
        }

        public void CreatePIS(PISDto pis)
        {
            try
            {
                PIS newPIS = PIS.Create(_mapper.Map<PISDto, PIS>(pis));
                if (newPIS != null)
                {
                    _pISRepository.Add(newPIS);
                    logCentral.Info(string.Format("PIS {0} has been created", newPIS.PISNo));
                    if (newPIS.IsSubmit == true)
                    {
                        //For Email testing purposes only - Begin
                        Dictionary<string, string> emailData = new Dictionary<string, string>();
                        emailData.Add("{RequestorName}", newPIS.Name);
                        emailData.Add("{Position}", newPIS.Position);
                        emailData.Add("{Department}", newPIS.DepartmentBU);
                        emailData.Add("{PISNumber}", newPIS.PISNo);
                        emailData.Add("{DateSubmitted}", DateTime.Now.ToLongDateString());
                        _emailManager.SetCalendarMessage();
                        _emailManager.SendMail(
                            new EmailSender(ConfigurationManager.AppSettings["EmailSender"]),
                            "pauljohn.villalobos@robinsonsretail.com.ph;ryan.cinco@robinsonsretail.com.ph",
                            "PIS Notification",
                            "PISNotification",
                            emailData);
                        //For Email testing purposes only - End
                    }

                }
                else
                {
                    logCentral.Info("Create PIS: Invalid PIS information");
                }
            }
            catch (Exception ex)
            {
                logCentral.Error("Create PIS", ex);
                throw;
            }
        }


        public PISDto getPISDetails(Guid PkId, int Id)
        {
            try
            {
                var pis = _mapper.Map<PIS, PISDto>(_pISRepository.getPISDetails(PkId, Id));
                return pis;
            }
            catch (Exception ex)
            {
                logCentral.Error("getPISDetails", ex);
                throw;
            }
        }


        public IEnumerable<PISDto> GetAll()
        {
            try
            {
                var pisList = _pISRepository.GetAll();
                return _mapper.Map<IEnumerable<PIS>, IEnumerable<PISDto>>(pisList);
            }
            catch (Exception ex)
            {
                logCentral.Error("GetAll", ex);
                throw;
            }
        }

        public void UpdatePIS(PISDto pis)
        {
            try
            {
                PIS newPIS = PIS.Update(_mapper.Map<PISDto, PIS>(pis));
                if (newPIS != null)
                {
                    _pISRepository.Update(newPIS);
                    logCentral.Info(string.Format("PIS {0} has been updated", newPIS.PISNo));

                    if (newPIS.IsSubmit == true)
                    {
                        //For Email testing purposes only - Begin
                        Dictionary<string, string> emailData = new Dictionary<string, string>();
                        emailData.Add("{RequestorName}", newPIS.Name);
                        emailData.Add("{Position}", newPIS.Position);
                        emailData.Add("{Department}", newPIS.DepartmentBU);
                        emailData.Add("{PISNumber}", newPIS.PISNo);
                        emailData.Add("{DateSubmitted}", DateTime.Now.ToLongDateString());
                        _emailManager.SendMail(
                            new EmailSender(ConfigurationManager.AppSettings["EmailSender"]),
                              "pauljohn.villalobos@robinsonsretail.com.ph;ryan.cinco@robinsonsretail.com.ph",
                            "PIS Notification",
                            "PISNotification",
                            emailData);
                        //For Email testing purposes only - End
                    }

                }
                else
                {
                    logCentral.Info("Update PIS: Invalid PIS information");
                }
            }
            catch (Exception ex)
            {
                logCentral.Error("Update PIS", ex);
                throw;
            }
        }

        public void UpdateIsBRS(PISDto pis)
        {
            try
            {
                PIS newPIS = PIS.Update(_mapper.Map<PISDto, PIS>(pis));
                if (newPIS != null)
                {
                    _pISRepository.UpdateIsBRS(newPIS);
                    logCentral.Info(string.Format("PIS {0} has been updated", newPIS.Pkid));

                }
                else
                {
                    logCentral.Info("Update PIS: Invalid PIS information");
                }
            }
            catch (Exception ex)
            {
                logCentral.Error("Update PIS", ex);
                throw;
            }
        }

        public void UpdateIsApproved(PISDto pis)
        {
            try
            {
                PIS newPIS = PIS.Update(_mapper.Map<PISDto, PIS>(pis));
                if (newPIS != null)
                {
                    _pISRepository.UpdateIsApproved(newPIS);
                    logCentral.Info(string.Format("PIS {0} has been updated", newPIS.PISNo));

                }
                else
                {
                    logCentral.Info("Update PIS: Invalid PIS information");
                }
            }
            catch (Exception ex)
            {
                logCentral.Error("UpdateIsApproved PIS", ex);
                throw;
            }
        }




        public void UpdateIsSubmit(int Id)
        {
            try
            {
               
                if (Id != 0)
                {
                   
                    _pISRepository.UpdateIsSubmit(Id);
                    var pisInfo = _pISRepository.getPISDetails(new Guid(),Id);
                    logCentral.Info(string.Format("PIS {0} has been submitted", Id));
                    //For Email testing purposes only - Begin
                    Dictionary<string, string> emailData = new Dictionary<string, string>();
                    emailData.Add("{RequestorName}", pisInfo.Name);
                    emailData.Add("{Position}", pisInfo.Position);
                    emailData.Add("{Department}", pisInfo.DepartmentBU);
                    emailData.Add("{PISNumber}", pisInfo.PISNo);
                    emailData.Add("{DateSubmitted}", DateTime.Now.ToLongDateString());
                    _emailManager.SendMail(
                        new EmailSender(ConfigurationManager.AppSettings["EmailSender"]),
                           "pauljohn.villalobos@robinsonsretail.com.ph;ryan.cinco@robinsonsretail.com.ph",
                        "PIS Notification",
                        "PISNotification",
                        emailData);
                    //For Email testing purposes only - End




                }
                else
                {
                    logCentral.Info("Update PIS: Invalid PIS information");
                }
            }
            catch (Exception ex)
            {
                logCentral.Error("UpdateIsSubmit PIS", ex);
                throw;
            }
        }


        public void DeletePIS(int id)
        {
            try
            {
                _pISRepository.Remove(id);
                logCentral.Info(string.Format("PIS with id = {0} has been deleted", id));

           

            }
            catch (Exception ex)
            {
                logCentral.Error("Remove PIS", ex);
                throw;
            }
        }



        public void UpdatePISRecommendation(PISDto pis)
        {
            try
            {
                PIS newPIS = PIS.Update(_mapper.Map<PISDto, PIS>(pis));
                if (newPIS != null)
                {
                    _pISRepository.UpdatePISRecommendation(newPIS);
                    logCentral.Info(string.Format("PIS {0} has been updated", newPIS.PISNo));
                }
                else
                {
                    logCentral.Info("UpdatePISRecommendation: Invalid PIS information");
                }
            }
            catch (Exception ex)
            {
                logCentral.Error("UpdatePISRecommendation", ex);
                throw;
            }
        }

<<<<<<< HEAD

        public void UpdateCabSchedule(PISDto pis)
        {
            try
            {
                PIS newPIS = PIS.Update(_mapper.Map<PISDto, PIS>(pis));
                var pisInfo = _pISRepository.getPISDetails(new Guid(), pis.Id);
                if (newPIS != null)
                {
                    _pISRepository.UpdateCabSchedule(newPIS);
                    logCentral.Info(string.Format("PIS {0} has been updated", pisInfo.PISNo));

                    //For Email testing purposes only - Begin
                    Dictionary<string, string> emailData = new Dictionary<string, string>();
                    emailData.Add("{CABNote}", pis.CabNote);
                    emailData.Add("{CABSchedule}", String.Format("{0:dddd, MMMM d, yyyy}", pis.CabReviewSchedule));
                    emailData.Add("{Name}", pisInfo.Name);
                    emailData.Add("{PISNo}", pisInfo.PISNo);
                    _emailManager.SendMail(
                        new EmailSender(ConfigurationManager.AppSettings["EmailSender"]),
                           "pauljohn.villalobos@robinsonsretail.com.ph;ryan.cinco@robinsonsretail.com.ph",
                        "CAB Review Schedule Notification",
                        "CABNotification",
                        emailData);
                    //For Email testing purposes only - End

                }
                else
                {
                    logCentral.Info("UpdatePISRecommendation: Invalid PIS information");
                }
            }
            catch (Exception ex)
            {
                logCentral.Error("UpdateCabSchedule", ex);
                throw;
            }
        }


        public void UpdateApprovedPIS(int Id)
        {
            try
            {
                if (Id != 0)
                {

                    _pISRepository.UpdateApprovedPIS(Id);
                    logCentral.Info(string.Format("PIS {0} has been submitted", Id));
                    var pisInfo = _pISRepository.getPISDetails(new Guid(), Id);
                    //For Email testing purposes only - Begin
                    Dictionary<string, string> emailData = new Dictionary<string, string>();
                    emailData.Add("{Name}", pisInfo.Name);
                    emailData.Add("{PISNumber}", pisInfo.PISNo);
                    emailData.Add("{ApproverName}", "Administrator");
                    emailData.Add("{DateApproved}", String.Format("{0:dddd, MMMM d, yyyy}", DateTime.Now));
                    _emailManager.SendMail(
                        new EmailSender(ConfigurationManager.AppSettings["EmailSender"]),
                           "pauljohn.villalobos@robinsonsretail.com.ph;ryan.cinco@robinsonsretail.com.ph",
                        "Approved PIS Notification",
                        "ApprovedPISNotification",
                        emailData);
                    //For Email testing purposes only - End
                }
                else
                {
                    logCentral.Info("Update PIS: Invalid PIS information");
                }
            }
            catch (Exception ex)
            {
                logCentral.Error("UpdateIsSubmit PIS", ex);
                throw;
            }
        }





        public void UpdateSendBackPIS(PISDto pis)
        {
            try
            {
                PIS newPIS = PIS.Update(_mapper.Map<PISDto, PIS>(pis));
                var pisInfo = _pISRepository.getPISDetails(new Guid(), pis.Id);
                if (newPIS != null)
                {
                    _pISRepository.UpdateSendBackPIS(newPIS);
                    logCentral.Info(string.Format("PIS {0} has been updated", pisInfo.PISNo));


                    //For Email testing purposes only - Begin
                    Dictionary<string, string> emailData = new Dictionary<string, string>();
                    emailData.Add("{SendBackNote}", pis.SendBackNote);
                    emailData.Add("{Name}", pisInfo.Name);
                    emailData.Add("{PISNo}", pisInfo.PISNo);
                    _emailManager.SendMail(
                        new EmailSender(ConfigurationManager.AppSettings["EmailSender"]),
                           "pauljohn.villalobos@robinsonsretail.com.ph;ryan.cinco@robinsonsretail.com.ph",
                        "PIS Rejected Notificaton",
                        "PISSendBackNotification",
                        emailData);
                    //For Email testing purposes only - End

                }
                else
                {
                    logCentral.Info("UpdateSendBackPIS: Invalid PIS information");
                }
            }
            catch (Exception ex)
            {
                logCentral.Error("UpdateSendBackPIS", ex);
                throw;
            }
        }



        public IEnumerable<PISDto> getListStatus(string listname)
        {
            try
            {
                var projectList = _pISRepository.getListStatus(listname);
                return _mapper.Map<IEnumerable<PIS>, IEnumerable<PISDto>>(projectList);
            }
            catch (Exception ex)
            {
                logCentral.Error("GetAll", ex);
                throw;
            }
        }

        //public PISDto getListStatus(string listname)
        //{
        //    try
        //    {
        //        var pis = _mapper.Map<PIS, PISDto>(_pISRepository.getListStatus(listname));
        //        return pis;
        //    }
        //    catch (Exception ex)
        //    {
        //        logCentral.Error("getListStatus", ex);
        //        throw;
        //    }
        //}



=======
>>>>>>> e0c77c851bfb9fa3f68ef227cf7bc87ed6aca07c
        public void Dispose()
        {
            _pMSUnitOfWork.Dispose();
        }
    }
}
