using AutoMapper;
using PMS.ApplicationService.DTOs;
using PMS.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS.Web
{
    public class WebMappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<BRSDto, BRSModel>().ReverseMap();
            CreateMap<PISDto, PISInformation>().ReverseMap();
            CreateMap<PISDevDto, PISDevModel>().ReverseMap();
            CreateMap<PISIncrementNoDto, PISIncrementNoModel>().ReverseMap();
            CreateMap<BRSIncrementNoDto, BRSIncrementNoModel>().ReverseMap();
        }
    }
}
