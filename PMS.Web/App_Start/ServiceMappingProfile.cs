using AutoMapper;
using PMS.ApplicationService.DTOs;
using PMS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMS.Web
{
    public class ServiceMappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<BRSEIncrementNo, BRSIncrementNoDto>().ReverseMap();
            CreateMap<PISEIncrementNo, PISIncrementNoDto>().ReverseMap();
            CreateMap<PIS, PISDto>().ReverseMap();
            CreateMap<PISDev, PISDevDto>().ReverseMap();
            CreateMap<BRS, BRSDto>().ReverseMap();
        }
    }
}
