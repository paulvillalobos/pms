using DomainEntity = PMS.Domain.Entities;
using pms = PMS.Infrastructure.EntityFramework.PMS;
using Common = PMS.Infrastructure.EntityFramework.COMMON;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//using PMS = PMS.Infrastructure.PMS;
//using Common = PMS.Infrastructure.COMMON;

namespace PMS.Web
{
    public class DomainMappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<DomainEntity.BRSEIncrementNo, pms.BRSIncrementNo>().ReverseMap();
            CreateMap<DomainEntity.PISEIncrementNo, pms.PISIncrementNo>().ReverseMap();
            CreateMap<DomainEntity.PIS, pms.PI>().ReverseMap();
            CreateMap<DomainEntity.PISDev, pms.PISDev>().ReverseMap();
            CreateMap<DomainEntity.BRS, pms.BR>().ReverseMap();
        }
    }
}
