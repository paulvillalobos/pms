using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc4;
using PMS.ApplicationService.ServiceContract;
using PMS.ApplicationService.Services;
using AutoMapper;
using PMS.Domain.Contracts;
using PMS.Infrastructure.Repository;
using PMS.Infrastructure.EntityFramework.PMS;
using PMS.Infrastructure.EntityFramework.COMMON;
using PMS.Common.EmailHelper;

namespace PMS.Web
{
    public static class Bootstrapper
    {
        public static IUnityContainer Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            return container;
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();    

            // Service Registry
            // Repository Registry
            //EPortal ----------------------------------------------------------------------------------------------------
            container.RegisterType<IPMSUnitOfWork, PMSUnitOfWork>(new HierarchicalLifetimeManager());
            container.RegisterType<ICommonUnitOfWork, CommonUnitOfWork>(new HierarchicalLifetimeManager());


            container.RegisterType<IBRSIncrementNoService, BRSIncrementNoService>();
            container.RegisterType<IEmailManager, EmailManager>();
            container.RegisterType<IBRSService, BRSService>();
            container.RegisterType<IPISService, PISService>();
            container.RegisterType<IPISDevService, PISDevService>();
            container.RegisterType<IPISIncrementNoService, PISIncrementNoService>();

            //// Repository Registry
            container.RegisterType<IBRSIncrementNo, BRSIncrementNoRepository>();
            container.RegisterType<IBRSRepository, BRSRepository>();
            container.RegisterType<IPISDevRepository, PISDevRepository>();
            container.RegisterType<IPISIncrementNo, PISIncrementNoRepository>();
            container.RegisterType<IPISRepository, PISRepository>();

            // AutoMapper Registry
            container.RegisterInstance<IMapper>(AutoMapperConfig().CreateMapper());

            RegisterTypes(container);

            return container;
        }

        public static void RegisterTypes(IUnityContainer container)
        {

        }

        public static MapperConfiguration AutoMapperConfig()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new WebMappingProfile());
                cfg.AddProfile(new ServiceMappingProfile());
                cfg.AddProfile(new DomainMappingProfile());
            });
        }
    }
}
