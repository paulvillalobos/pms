﻿$(document).ready(function () {
    $(function () {
        GetList();
    });
});
function GetList() {
    PageLoader();

    $("#tblcontentLoading").html("<p style='font-size: 16px; font-weight: bold;'><i class='fas fa-circle-notch fa-spin' style='color: red; font-size: 24px;'></i></p>");
    $.ajax({
        type: "GET",
        url: '/Product/List',
        dataType: "html",
        success: function (data) {
 
            $("#tblcontentLoading").remove();
            $("#tblcontent").empty();
            $("#tblcontent").html(data);
            $("#tblcontent").fadeIn('slow');
            $("#loading").hide();      
        },
        error: function (data) {
            $("#tblcontent").empty();
            $('.modal-backdrop').remove();
        }
    });

   
};




ShowErrorTrace = function () {
    $('#errorTrace').show();
}

DisplayErrorMessage = function (error) {
    alert(error.responseText);
}

//$(document).ready(function () {
//    //Call EmpDetails jsonResult Method  
//    $.getJSON("Product/ProductList",
//        function (json) {
//            var tr;
//            //Append each row to html table  
//            for (var i = 0; i < json.length; i++) {
//                tr = $('<tr/>');
//                tr.append("<td>" + json[i].ProductName + "</td>");
//                tr.append("<td>" + json[i].Type + "</td>");
//                if (json[i].Status == "True") {
//                    tr.append("<td>Active</td>");
//                }
//                else {
//                    tr.append("<td>Not Active</td>");
//                }

//                tr.append("<td>" + '<a href="javascript:void(0);" class="anchorDetail" data-id=""' + json[i].recordId +'"" data-name="edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit text text-muted"></i></a> & nbsp;|' + "</td>");
//                tr.append("<td>" + '<a href="javascript:void(0);" class="anchorDetail" data-id=""' + json[i].recordId +'"" data-name="detail" data-toggle="tooltip" title="View Details"><i class="fa fa-eye text text-muted"></i></a> & nbsp;|' + "</td>");
//                tr.append("<td>" + '<a href="javascript:void(0);" class="anchorConfirm" data-id=""'+ json[i].recordId +'"" data-name="delete" data-action="Product/Delete"><i class="fa fa-trash text text-danger" data-toggle="tooltip" title="Delete "></i></a>' + "</td>");
//                $('table').append(tr);
//            }
//        });
//});  



    //<a href="javascript:void(0);" class="anchorDetail" data-id="@item.RecordId" data-name="detail" data-toggle="tooltip" title="View Details"><i class="fa fa-eye text text-muted"></i></a> & nbsp;|
    //    <a href="javascript:void(0);" class="anchorConfirm" data-id="@item.RecordId" data-name="delete" data-action="Product/Delete"><i class="fa fa-trash text text-danger" data-toggle="tooltip" title="Delete "></i></a>
