﻿$(document).ready(function () {
    
});

CreateEmployee = function() {
    var data = CreateJsonData([
        "EmployeeId",
        "FirstName",
        "MiddleName",
        "LastName",
        "Status",
        "AccessNo"]);
    $.post('/Employee/Create', data, function (res) {
        window.location.href = res.redirectToUrl;
    }).fail(function (res) {
        DisplayErrorMessage(res);
    });
}

UpdateEmployee = function () {
    var data = CreateJsonData([
        "EmployeeId",
        "FirstName",
        "MiddleName",
        "LastName",
        "Status",
        "AccessNo",
        "BadgeId"]);
     $.post('/Employee/Update', data, function (res) {
         window.location.href = res.redirectToUrl;
     }).fail(function (res) {
         DisplayErrorMessage(res);
     });
}

DeleteEmployee = function (strPkid) {
    $.post('/Employee/DeleteEmployee', { Pkid: strPkid }, function (res) {
        window.location.href = res.redirectToUrl;
    }).fail(function (res) {
        DisplayErrorMessage(res);
    });
}

CreateJsonData = function (targetFieldIds) {
    var tempData = "{data:[{";
    targetFieldIds.forEach(function (value, index) {
        tempData += '"' + value + '":"' + $("#" + value).val() + '"';
        if (targetFieldIds.length != index +1) {
            tempData += ",";
        }
    });
    tempData += "}]}";
    return tempData;
}