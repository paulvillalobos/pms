﻿$(document).ready(function () {
    //$("#idSave").click(function (e) {
    $("#idSave").on("click", function (e) {
        e.preventDefault();

        var PISNo = $("#idPISNo").val();
        var ApprovedDate = $("#idApprovedDate").val();
        var Notes = $("#idNotes").val();
        var BU = $("#idBU").val();

        if (PISNo === "" || ApprovedDate === "" || Notes === "" || BU === "" || $("#idDocument").get(0).files.length === 0) {
            if (PISNo === "") $('#idPISNo').css('border-color', 'red');
            if (ApprovedDate === "") $('#idApprovedDate').css('border-color', 'red');
            if (Notes === "") $('#idNotes').css('border-color', 'red');
            if (BU === "") $('#idBU').css('border-color', 'red');
            if ($("#idDocument").get(0).files.length === 0) $('#idDocument').css('border-color', 'red');
        } else {
           
        }
    });

    $("#idSubmit").on("click", function (e) {
        e.preventDefault();
        var PISNo = $("#idPISNo").val();
        var ApprovedDate = $("#idApprovedDate").val();
        var Notes = $("#idNotes").val();
        var BU = $("#idBU").val();

        if (PISNo === "" || ApprovedDate === "" || Notes === "" || BU === "" || $("#idDocument").get(0).files.length === 0) {
            if (PISNo === "") $('#idPISNo').css('border-color', 'red');
            if (ApprovedDate === "") $('#idApprovedDate').css('border-color', 'red');
            if (Notes === "") $('#idNotes').css('border-color', 'red');
            if (BU === "") $('#idBU').css('border-color', 'red');
            if ($("#idDocument").get(0).files.length === 0) $('#idDocument').css('border-color', 'red');
        } else {
            CreateBRS();
        }
    });

    $("#idUpdate").on("click", function (e) {
        e.preventDefault();
        UpdateBRS();
    });
});

function CreateBRS() {
    var BRS = {
        BU: $("#idBU").val(),
        PISNo: $("#idPISNo").val(),
        BRSApprovedDate: $("#idApprovedDate").val(),
        Notes: $("#idNotes").val(),
    }

    $.ajax({
        type: "POST",
        url: $("#divCreateBRS").data("request-url"),
        data: JSON.stringify({ br: BRS }),
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            UploadPDFFile();
        },
        error: function (request, status, error) {
            alert(error);
        }
    });
}


function UploadPDFFile() {
    var formData = new FormData();
    var pisNo = $("#idPISNo").val();
    //var StoreCode = $("#idPromoStoreCodeID").val();
    //var itemNo = $("#idItemNo").val();
    var flagCont = 0;

    //Formdata here
    var fileControl = $("#" + "" + "idDocument").get(0);
    var files = fileControl.files;
    for (var i = 0; i < files.length; i++) {
        flagCont++;
        var file = files[i];
        formData.append('pdf', file);
    }

    if (flagCont > 0) {
        $.ajax({
            type: "POST",
            url: $("#divUploadCustRedeemedPDF").data("request-url") + "?pISBo=" + pisNo, //+ "&storeCode=" + StoreCode + "&itemNo=" + itemNo,
            data: formData,
            dataType: 'html',
            contentType: false,
            processData: false,
            success: function () {
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: $("#divGetPisNo").data("request-url"),
                    data: { pisNo: pisNo },
                    contentType: "application/json;charset=utf-8",
                    success: function (data) {
                        if (data != null) {
                            data = JSON.parse(JSON.stringify(data));

                            window.location.href = '/BRS/Details?Id=' + data.Id;
                        }
                        else {
                            alert("Data not found");
                        }
                    },

                    failure: function (response) {
                        alert(response.responseText);
                    },
                    error: function (response) {
                        alert(response.responseText);
                    }
                });
            },
            error: function (er) {
                alert(JSON.stringify(er));
            }
        });
    }
    else {
        //window.location.href = data.lstReturnUrl + "?asIcon=" + data.asIcon + "&asMessage=" + data.asMessage + "&aiBusinessUnitID=" + data.aiBusinessUnitID + "&aiDivisionID=" + data.aiDivisionID;
    }
}

function UpdateBRS() {
    var BRS = {
        'PISNo': $("#idUpdatePISNo").val(),
        'BRSNo': $("#idUpdateBRSNo").val(),
        'BRSApprovedDate': $("#idUpdateDateAppr").val(),
        'PISModel.AppName': $("#idUpdateApp").val(),
        'PISModel.Title': $("#idUpdateTitle").val(),
        'PISModel.ApprovedBy': $("#idUpdateApprBy").val(),
        'PISModel.AppDevNote': $("#idUpdateNotes").val(),
        'PISModel.AppDevComment': $("#idUpdateComment").val(), 
        'PISModel.DateApproved': $("#idUpdateDateAppr").val(),       
    }

    $.ajax({
        type: "POST",
        url: $("#divUpdateBRS").data("request-url"),
        data: JSON.stringify({ br: BRS }),
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            window.location.href = '/BRS/List/';
        },
        error: function (request, status, error) {
            alert(error);
        }
    });
}