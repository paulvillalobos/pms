﻿function PageLoader() {
    $("#loading").fadeIn();
    var opts = {
        lines: 12, // The number of lines to draw
        length: 7, // The length of each line
        width: 4, // The line thickness
        radius: 10, // The radius of the inner circle
        color: '#000', // #rgb or #rrggbb
        speed: 1, // Rounds per second
        trail: 60, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: false // Whether to use hardware acceleration
    };
    var target = document.getElementById('loading');
    var spinner = new Spinner(opts).spin(target);
  
}
$('.PageLoader').click(function () {
    PageLoader();
});
function PageLoader() {
    $("#loading").fadeIn();
   
}
$(window).on('beforeunload', function () {
    PageLoader();
    // save the values to server side
});

$(window).on('load', function () {
});



$('#detailModal').on('show.bs.modal', function (event) {
    // Fix Animate.css
    $('#parent_div_have_error').removeClass('fadeIn');
});

$('#detailModal').on('hidden.bs.modal', function (e) {
    // Fix Animate.css
    $('#parent_div_have_error').addClass('fadeIn');
});