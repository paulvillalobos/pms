﻿using AutoMapper;
using PMS.ApplicationService.DTOs;
using PMS.ApplicationService.ServiceContract;
using PMS.Common.Logger;
using PMS.Web.ActionFilters;
using PMS.Web.ViewModels;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;

namespace PMS.Web.Controllers
{
    public class PISController : Controller
    {
        //This is for testing only
        private readonly IPISService _pisService;
        private readonly IPISIncrementNoService _pisIncrementNoService;
        private readonly IMapper _mapper;

        public PISController(IPISService pisService, IMapper mapper, IPISIncrementNoService pisIncrementNoService)
        {
            if (pisService == null) throw new ArgumentNullException("PISService");
            if (pisIncrementNoService == null) throw new ArgumentNullException("IPISIncrementNoService");
            if (mapper == null) throw new ArgumentNullException("Mapper");
            _pisService = pisService;
            _pisIncrementNoService = pisIncrementNoService;
            _mapper = mapper;
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Recommendation(Guid PkId)
        {
            try
            {
                if (!string.IsNullOrEmpty(PkId.ToString()))
                {
                    var pisInformation = _mapper.Map<PISDto, PISInformation>(_pisService.getPISDetails(PkId, 0));
                    return View(pisInformation);
                }
            }
            catch (Exception ex)
            {

                TempData["errorMessage"] = "System Error: " + ex.Message;
            }
            return View();
        }

<<<<<<< HEAD
=======

>>>>>>> e0c77c851bfb9fa3f68ef227cf7bc87ed6aca07c
        public ActionResult Edit(Guid PkId)
        {
            try
            {
                if (!string.IsNullOrEmpty(PkId.ToString()))
                {
                    var pisInformation = _mapper.Map<PISDto, PISInformation>(_pisService.getPISDetails(PkId,0));
                    return View(pisInformation);
                }
            }
            catch (Exception ex)
            {

                TempData["errorMessage"] = "System Error: " + ex.Message;
            }
            return View();
        }

        public ActionResult List()
        {
            var pisList = _mapper.Map<IEnumerable<PISDto>, IEnumerable<PISInformation>>(_pisService.GetAll());

            bool isEmpty = !pisList.Any();
            if (isEmpty)
            {
                ViewBag.countLst = "0";
                return View(pisList);

            }
            else
            {
                return View(pisList);
            }

            return View();

        }

        public ActionResult Details(Guid PkId)
        {
            try
            {
                if (!string.IsNullOrEmpty(PkId.ToString()))
                {
                    var pisInformation = _mapper.Map<PISDto, PISInformation>(_pisService.getPISDetails(PkId,0));
                    return View(pisInformation);
                }
            }
            catch (Exception ex)
            {

                TempData["errorMessage"] = "System Error: " + ex.Message;
            }
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult CreatePIS(PISInformation pi, string btnDraft, string btnSubmit)
        {
            try
            {

                bool bDraft = false;
                bool bSubmit = false;
                string message = "";
                if (btnDraft != null)
                {
                    bDraft = true;
                    message = "Record has been successfully saved as draft";
                }
                if (btnSubmit != null)
                {
                    bSubmit = true;
                    message = "Record has been successfully submitted";
                }

               // if (ModelState.IsValid)
                //{
                    var pisNo = _mapper.Map<PISIncrementNoDto, PISIncrementNoModel>(_pisIncrementNoService.GetPISNo());
                    DateTime now = DateTime.Now;
                    PISDto newPIS = new PISDto
                    {

                        PkId = Guid.NewGuid(),
                        Name = pi.Name,
                        PISNo = now.Year.ToString() + "-" + pisNo.PISNodisplay,
                        Position = pi.Position,
                        DepartmentBU = pi.DepartmentBU,
                        Manager = pi.Manager,
                        BusinessObjective = pi.BusinessObjective,
                        Request = pi.Request,
                        IsDraft = bDraft,
                        IsSubmit = bSubmit,
                        DateCreated = DateTime.Now,
                        RequestStatus = "N",
                    };
                    _pisService.CreatePIS(newPIS);
                    _pisIncrementNoService.UpdatePISNo();
                    TempData["alertMessage"] = message; 
                    return RedirectToAction("Details", new { PkId = newPIS.PkId });
                //}
            }
            catch (Exception ex)
            {
                TempData["errorMessage"] = "System Error: " + ex.Message;
            }
            return View(pi);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult UpdatePIS(PISInformation pi, string btnDraft, string btnSubmit)
        {
            try
            {

                bool bDraft = false;
                bool bSubmit = false;
                string message = "";
                if (btnDraft != null)
                {
                    bDraft = true;
                    message = "Record has been successfully saved as draft";
                }
                if (btnSubmit != null)
                {
                    bSubmit = true;
                    message = "Record has been successfully submitted";
                }

                //if (ModelState.IsValid)
                //{
                    PISDto newPIS = new PISDto
                    {
                        Id = pi.Id,
                        PkId = pi.PkId,
                        Name = pi.Name,
                        Position = pi.Position,
                        DepartmentBU = pi.DepartmentBU,
                        Manager = pi.Manager,
                        BusinessObjective = pi.BusinessObjective,
                        Request = pi.Request,
                        IsDraft = bDraft,
                        IsSubmit = bSubmit,
                        PISNo = pi.PISNo

                    };
                    _pisService.UpdatePIS(newPIS);
                    TempData["alertMessage"] = message;
                    return RedirectToAction("Details", new { PkId = newPIS.PkId });
                //}
            }
            catch (Exception ex)
            {
                TempData["errorMessage"] = "System Error: " + ex.Message;
            }
            return View(pi);
        }

        public ActionResult Delete(int id)
        {
            try
            {
                //Thread.Sleep(3000);
                _pisService.DeletePIS(id);
                TempData["alertMessage"] = "Record has been successfully deleted";
                //return this.Json(id, JsonRequestBehavior.AllowGet);
                string url = Request.UrlReferrer.ToString();
                return Json(new { redirectToUrl = url }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return View("List");
                TempData["errorMessage"] = "System Error: " + ex.Message;
            }
            // return RedirectToAction("List");
        }

        public ActionResult FlagSubmitPIS(int Id)
        {
            try
            {

                _pisService.UpdateIsSubmit(Id);
               // var pisInfo = _pisService.getPISDetails(new Guid(),Id);
                TempData["alertMessage"] = "Record has been successfully submitted";
                return Redirect(Request.UrlReferrer.ToString());
            }
            catch (Exception ex)
            {
                return View("List");
                TempData["errorMessage"] = "System Error: " + ex.Message;
            }
            // return RedirectToAction("List");
        }

        [HttpPost]
        public ActionResult SaveRecommendation(PISInformation pi)
        {
            try
            {

                PISDto newPIS = new PISDto
                {
                    Id = pi.Id,
                    PkId = pi.PkId,
                    AppType = pi.AppType,
                    ProjectWeight = pi.ProjectWeight,
                    AppDevTeam = pi.AppDevTeam,
                    AppManager = pi.AppManager,
                    AppDev = pi.AppDev,
                    SolutionRecommendation = pi.SolutionRecommendation,
                };
                _pisService.UpdatePISRecommendation(newPIS);

                TempData["alertMessage"] = "Additional details has been added";
                return Redirect(Request.UrlReferrer.ToString());
                //return RedirectToAction("Details", new { PkId = newPIS.PkId, role = "admin" });

<<<<<<< HEAD
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult SendAcknowledgement(PISInformation pi)
        {
            try
            {
                PISDto newPIS = new PISDto
                {
                    Id = pi.Id,
                    CabReviewSchedule = pi.CabReviewSchedule,
                    CabNote = pi.CabNote,
                    RequestStatus = "C",

                };
                _pisService.UpdateCabSchedule(newPIS);
                TempData["alertMessage"] = "CAB schedule has been added and sent to requestor";
                return Redirect(Request.UrlReferrer.ToString());
            }
            catch (Exception ex)
            {
                return View("List");
                TempData["errorMessage"] = "System Error: " + ex.Message;
            }

=======
        [HttpPost]
        public ActionResult SaveRecommendation(PISInformation pi)
        {
            try
            {

                PISDto newPIS = new PISDto
                {
                    Id = pi.Id,
                    PkId = pi.PkId,
                    AppType = pi.AppType,
                    ProjectWeight = pi.ProjectWeight,
                    AppDevTeam = pi.AppDevTeam,
                    AppManager = pi.AppManager,
                    //AppDev = pi.AppDev,
                    SolutionRecommendation = pi.SolutionRecommendation,
                };
                _pisService.UpdatePISRecommendation(newPIS);
                TempData["alertMessage"] = "Additional details has been added";
                return RedirectToAction("Details", new { PkId = newPIS.PkId });

            }
            catch (Exception)
            {

                throw;
            }
>>>>>>> e0c77c851bfb9fa3f68ef227cf7bc87ed6aca07c
        }

        public ActionResult ApprovedPIS(int Id)
        {
            try
            {

                _pisService.UpdateApprovedPIS(Id);
                // var pisInfo = _pisService.getPISDetails(new Guid(),Id);
                TempData["alertMessage"] = "PIS has been approved";
                string url = Request.UrlReferrer.ToString();
                return Json(new { redirectToUrl = url }, JsonRequestBehavior.AllowGet);

               // return Redirect(Request.UrlReferrer.ToString());
            }
            catch (Exception ex)
            {
                return View("List");
                TempData["errorMessage"] = "System Error: " + ex.Message;
            }
            // return RedirectToAction("List");
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult SendBackPIS(PISInformation pi)
        {
            try
            {
                PISDto newPIS = new PISDto
                {
                    Id = pi.Id,
                    SendBackNote = pi.SendBackNote,
                    DateSendBack = DateTime.Now,
                    IsSendBack = true,
                    SendBackBy = "Administrator",
                    RequestStatus = "E",
            };
                _pisService.UpdateSendBackPIS(newPIS);
                TempData["alertMessage"] = "PIS has been send back to the requestor";
                return Redirect(Request.UrlReferrer.ToString());
            }
            catch (Exception ex)
            {
                return View("List");
                TempData["errorMessage"] = "System Error: " + ex.Message;
            }

        }



        public ActionResult ProjectList(string listname)
        {
            var projectList = _mapper.Map<IEnumerable<PISDto>, IEnumerable<PISInformation>>(_pisService.getListStatus(listname));

            bool isEmpty = !projectList.Any();
            if (isEmpty)
            {

                ViewBag.countPrjLst = "0";
                return View(projectList);

            }
            else
            {
                return View(projectList);
            }

            return View();

        }

        //public ActionResult getListStatus(string  listname)
        //{
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(listname))
        //        {
        //            var projectList = _mapper.Map<PISDto, PISInformation>(_pisService.getListStatus(listname));
        //            return View(projectList);
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        TempData["errorMessage"] = "System Error: " + ex.Message;
        //    }
        //    return View();
        //}





            //[JsonFilter(Param = "data", JsonDataType = typeof(IList<PISInformation>), PropertyName = "data")]
            //[HttpPost]
            //public JsonResult CreatePIS(IList<Dictionary<string, string>> data)
            //{
            //    try
            //    {
            //        PISDto newPIS = new PISDto
            //        {
            //            Name = data[0]["Name"],
            //            Position = data[0]["Position"],
            //            DepartmentBU = data[0]["DepartmentBU"],
            //            Manager = data[0]["Manager"],
            //            BusinessObjective = data[0]["BusinessObjective"],
            //            Request = data[0]["Request"],
            //        };
            //        _pisService.CreatePIS(newPIS);
            //        //string url = "Details?employeeId=" + newEmployee.EmployeeId;
            //        // string url = "Details?Id=1";
            //        // return Json(new { redirectToUrl = "http://www.codeproject.com/" }, JsonRequestBehavior.AllowGet);
            //        ///return new JsonResult() { Data = new { redirectToUrl = url } };
            //        //return RedirectToAction("Uploaded", new { Id = newPIS.Name });


            //        string url = "Details?Id=" + newPIS.Name;
            //        return new JsonResult() { Data = new { redirectToUrl = url } };
            //    }
            //    catch (Exception ex)
            //    {
            //        throw ex;
            //    }
            //}

        }
}
