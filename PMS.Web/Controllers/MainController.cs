using AutoMapper;
using PMS.ApplicationService.DTOs;
using PMS.ApplicationService.ServiceContract;
using PMS.Common.Logger;
using PMS.Web.ActionFilters;
using PMS.Web.ViewModels;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;

namespace PMS.Web.Controllers
{
    public class MainController : Controller
    {
        private readonly IPISService _pisService;
        private readonly IBRSService _brsService;
        private readonly IMapper _mapper;

        public MainController(IPISService pisService, IMapper mapper, IBRSService brsService)
        {
            if (pisService == null) throw new ArgumentNullException("PISService");
            if (brsService == null) throw new ArgumentNullException("BRSService");
            if (mapper == null) throw new ArgumentNullException("Mapper");
            _pisService = pisService;
            _brsService = brsService;
            _mapper = mapper;
        }

        public ActionResult Dashboard()
        {

            var countNew = _mapper.Map<IEnumerable<PISDto>, IEnumerable<PISInformation>>(_pisService.getListStatus("new"));
            ViewBag.countNew = countNew.Count();

            var countCab = _mapper.Map<IEnumerable<PISDto>, IEnumerable<PISInformation>>(_pisService.getListStatus("cab"));
            ViewBag.countCab = countCab.Count();

            var countPis = _mapper.Map<IEnumerable<PISDto>, IEnumerable<PISInformation>>(_pisService.getListStatus("pis"));
            ViewBag.countPis = countPis.Count();

            var countAlladmin = _mapper.Map<IEnumerable<PISDto>, IEnumerable<PISInformation>>(_pisService.getListStatus("all"));
            ViewBag.countAlladmin = countAlladmin.Count();

            var countAlluser = _mapper.Map<IEnumerable<PISDto>, IEnumerable<PISInformation>>(_pisService.GetAll());
            ViewBag.countAlluser = countAlluser.Count();

            var countBRS = _mapper.Map<IEnumerable<BRSDto>, IEnumerable<BRSModel>>(_brsService.GetAll());
            ViewBag.countBRS = countBRS.Count();


            // A array of authors  
            string[] divColor = { "warning", "success", "danger", "info", "primary"};
            // Create a Random object  
            Random rand = new Random();
            // Generate a random index less than the size of the array.  
            int index = rand.Next(divColor.Length);
            // Display the result.  
            string divPickColor = divColor[index].ToString();
            ViewBag.divPickColor = divPickColor;
            return View();
        }

    }
}
