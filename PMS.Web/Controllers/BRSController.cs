﻿using AutoMapper;

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.IO;
using System.Web.Security;
using System.Configuration;

using PMS.ApplicationService.DTOs;
using PMS.ApplicationService.ServiceContract;
using PMS.Common.Logger;
using PMS.Web.ActionFilters;
using PMS.Web.ViewModels;

namespace PMS.Web.Controllers
{
    public class BRSController : Controller
    {
        private readonly IBRSService _brsService;
        private readonly IPISService _pISService;
        private readonly IBRSIncrementNoService _bRSIncrementNoService;
        private readonly IMapper _mapper;

        public BRSController(IBRSService brsService, IMapper mapper, IBRSIncrementNoService bRSIncrementNoService, IPISService pISService)
        {
            if (brsService == null) throw new ArgumentNullException("BRSService");
            if (bRSIncrementNoService == null) throw new ArgumentNullException("BRSIncrementNoService");
            if (pISService == null) throw new ArgumentNullException("PISService");
            if (mapper == null) throw new ArgumentNullException("Mapper");
            _brsService = brsService;
            _bRSIncrementNoService = bRSIncrementNoService;
            _pISService = pISService;
            _mapper = mapper;
        }
        //
        // GET: /BRS/

        public ActionResult Create()
        {
            //var pisInformation = _mapper.Map<BRSIncrementNoDto, BRSIncrementNoModel>(_bRSIncrementNoService.GetBRSNo());
            return View();
        }

        public ActionResult Edit(int id)
        {
            try
            {
                if (id != null)
                {
                    var pisInformation = _mapper.Map<BRSDto, BRSModel>(_brsService.getBRSDetails(id));
                    return View(pisInformation);
                }
            }
            catch (Exception ex)
            {

                TempData["errorMessage"] = "System Error: " + ex.Message;
            }
            return View();
        }

        [HttpGet]
        public ActionResult List()
        {
            var pisList = _mapper.Map<IEnumerable<BRSDto>, IEnumerable<BRSModel>>(_brsService.GetAll());

            bool isEmpty = !pisList.Any();
            if (isEmpty)
            {
                ViewBag.countLst = "0";
                return View(pisList);
            }
            else
            {
                return View(pisList);
            }

            return View();

        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            try
            {
                if (id != null)
                {
                    var pisInformation = _mapper.Map<BRSDto, BRSModel>(_brsService.getBRSDetails(id));
                    return View(pisInformation);
                }
            }
            catch (Exception ex)
            {

                TempData["errorMessage"] = "System Error: " + ex.Message;
            }
            return View();
        }

        [HttpGet]
        public ActionResult GetPisNo(string pisNo)
        {
            try
            {
                if (pisNo != null)
                {
                    var pisInformation = _mapper.Map<BRSDto, BRSModel>(_brsService.GetPISNo(pisNo));
                    return Json(pisInformation, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                TempData["errorMessage"] = "System Error: " + ex.Message;
            }
            return View();
        }

        [HttpPost]
        public void CreateBRS(BRSModel br)
        {
            try
            {
                var pisInformation = _mapper.Map<BRSIncrementNoDto, BRSIncrementNoModel>(_bRSIncrementNoService.GetBRSNo());

                BRSDto newBRS = new BRSDto
                {
                    PISNo = br.PISNo,
                    BRSNo = pisInformation.BRSNodisplay + "-" +  DateTime.Now.Year,
                    Notes = br.Notes,
                    BU = br.BU,
                    BRSApprovedDate = br.BRSApprovedDate,
                    FilePath = br.FilePath,
                    Filename = br.Filename,
                    Status = br.Status,
                    CreatedBy = br.CreatedBy,
                    DateCreated = DateTime.Now
                };
                _brsService.CreateBRS(newBRS);

                _bRSIncrementNoService.UpdateBRSNo();

                PISDto newPIS = new PISDto
                {
                    PISNo = br.PISNo,
                };
                _pISService.UpdateIsBRS(newPIS);

                //var q = _brsService.GetPISNo(br.PISNo);

                //string url = "Details?Id=" + q.Id;
                //return new JsonResult() { Data = new { redirectToUrl = url } };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public void UploadCustRedeemedPDF(string pISBo)
        {
            try
            {
                var pdfFileTemp = pISBo;

                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    string extension = Path.GetExtension(hpf.FileName);
                    string BaseDir = string.Concat(ConfigurationManager.AppSettings["FileServerBaseDir"], "FILE\\");
                    string uploadFile = BaseDir + pdfFileTemp + "-" + hpf.FileName + ".pdf";

                    byte[] bytes;
                    using (BinaryReader br = new BinaryReader(hpf.InputStream))
                    {
                        bytes = br.ReadBytes(hpf.ContentLength);
                    }

                    BRSDto upBRS = new BRSDto
                    {
                        PISNo = pISBo,
                        FilePath = uploadFile,
                        Filename = pdfFileTemp + "-" + hpf.FileName,
                    };
                    _brsService.UpdatePISNo(upBRS);

                    //Create Folder if not exist
                    if (!Directory.Exists(BaseDir))
                    {
                        Directory.CreateDirectory(BaseDir);
                    }

                    if (System.IO.File.Exists(uploadFile))
                    {
                        System.IO.File.Delete(uploadFile);
                        hpf.SaveAs(uploadFile);
                    }
                    else
                    {
                        hpf.SaveAs(uploadFile);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ActionResult DownloadBRSTemplate()
        {
            try
            {

                string filename = "2019-0000000024-BRS_PROJECT MONITORING SYSTEM.pdf" + ".pdf";
                string BaseDir = string.Concat(ConfigurationManager.AppSettings["FileServerBaseDir"], "FILE\\");
                string path = "";
                var content_type = "";
                path = Path.Combine(BaseDir, filename);

                if (filename.Contains(".pdf"))
                {
                    content_type = "application/pdf";
                }

                return File(path, content_type, filename);

            }
            catch (Exception ex)
            {
                TempData["errorMessage"] = "DownloadTemplate Error: " + ex.Message;
            }
            return View();
        }


        public ActionResult DownloadBRSAttachment(string fileName)
        {
            try
            {
                string filename = fileName + ".pdf";
                string BaseDir = string.Concat(ConfigurationManager.AppSettings["FileServerBaseDir"], "FILE\\");
                string path = "";
                var content_type = "";
                path = Path.Combine(BaseDir, filename);

                if (filename.Contains(".pdf"))
                {
                    content_type = "application/pdf";
                }

                return File(path, content_type, filename);
            }
            catch (Exception ex)
            {
                TempData["errorMessage"] = "DownloadTemplate Error: " + ex.Message;
            }
            return View();
        }


        [HttpPost]
        public void UpdateBRS(BRSModel br)
        {
            try
            {
                BRSDto newBRS = new BRSDto
                {
                    BRSNo = br.BRSNo,
                    BRSApprovedDate = br.BRSApprovedDate,
                    //FilePath = br.FilePath,
                    //Filename = br.Filename,
                    Status = br.Status,
                    CreatedBy = br.CreatedBy,
                    DateCreated = DateTime.Now
                };
                _brsService.UpdateBRS(newBRS);

                PISDto UpdatePIS = new PISDto
                {
                    PISNo = br.PISNo,
                    AppName = br.PISModel.AppName,
                    Title = br.PISModel.Title,
                    AppDevNote = br.PISModel.AppDevNote,
                    AppDevComment = br.PISModel.AppDevComment,
                    ApprovedBy = br.PISModel.ApprovedBy,
                    DateApproved = br.PISModel.DateApproved,
                };
                _pISService.UpdateIsApproved(UpdatePIS);

                TempData["alertMessage"] = "Record has been successfully updated";
            }
            catch (Exception ex)
            {
                TempData["errorMessage"] = "System Error: " + ex.Message;
            }
        }


        public void Delete(int id)
        {
            try
            {
                Thread.Sleep(3000);
                _brsService.DeleteBRS(id);
                TempData["alertMessage"] = "Record has been successfully deleted";
            }
            catch (Exception ex)
            {
                //return View("List");
                TempData["errorMessage"] = "System Error: " + ex.Message;
            }
        }

    }
}
