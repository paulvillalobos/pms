﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PMS.Web.ViewModels
{
    public class PISDevModel
    {
        public int Id { get; set; }
        public string PISNo { get; set; }
        public string DevName { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> DateEdited { get; set; }
        public Nullable<bool> Deleted { get; set; }
        public string DeletedBy { get; set; }
        public Nullable<System.DateTime> DateDeleted { get; set; }
    }
}