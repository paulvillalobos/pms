﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PMS.Web.ViewModels
{
    public class PISIncrementNoModel
    {
        public int Id { get; set; }
        public int PISNo { get; set; }
        public string PISNodisplay { get; set; }
    }
}