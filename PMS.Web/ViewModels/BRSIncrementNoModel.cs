﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PMS.Web.ViewModels
{
    public class BRSIncrementNoModel
    {
        public int Id { get; set; }
        public int BRSNo { get; set; }
        public string BRSNodisplay { get; set; }
    }
}