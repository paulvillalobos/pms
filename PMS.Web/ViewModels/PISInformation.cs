using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PMS.Web.ViewModels
{
    public class PISInformation
    {
     
        public int Id { get; set; }
        public string PISNo { get; set; }
        [Required(ErrorMessage = "This is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "This is required")]
        public string DepartmentBU { get; set; }
        [Required(ErrorMessage = "This is required")]
        public string Position { get; set; }
        [Required(ErrorMessage = "This is required")]
        public string Manager { get; set; }
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "This is required")]
        public string BusinessObjective { get; set; }
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "This is required")]
        public string Request { get; set; }
        public string SolutionRecommendation { get; set; }
        public Nullable<System.DateTime> DateReceived { get; set; }
        public string ReceivedBy { get; set; }
        public Nullable<System.DateTime> DateViewed { get; set; }
        public string ReviewedBy { get; set; }
        public string RequestStatus { get; set; }
        public string ProjectWeight { get; set; }
        public string AppDevTeam { get; set; }
        public string AppManager { get; set; }
        public string Title { get; set; }
        public string AppName { get; set; }
        public string AppType { get; set; }
        public string AppRequestor { get; set; }
        public string AppDevComment { get; set; }
        public string AppDevNote { get; set; }
        public Nullable<System.DateTime> DateSubmitted { get; set; }
        [Required(ErrorMessage = "This is required")]
        public Nullable<System.DateTime> CabReviewSchedule { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public Nullable<bool> IsBRS { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> DateEdited { get; set; }
        public Nullable<bool> Deleted { get; set; }
        public string DeletedBy { get; set; }
        public Nullable<System.DateTime> DateDeleted { get; set; }
        public Nullable<bool> IsDraft { get; set; }
        public Nullable<bool> IsSubmit { get; set; }
        public System.Guid PkId { get; set; }
        public Nullable<bool> IsApproved { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> DateApproved { get; set; }
        public string AppDev { get; set; }
        public Nullable<bool> IsApprovedPIS { get; set; }
        public Nullable<System.DateTime> DateApprovedPIS { get; set; }
        public string ApprovedPISBy { get; set; }
        [Required(ErrorMessage = "This is required")]
        public Nullable<bool> IsSendBack { get; set; }
        public Nullable<System.DateTime> DateSendBack { get; set; }
        public string SendBackBy { get; set; }

        public string CabNote { get; set; }
        public string SendBackNote { get; set; }

        public string BRSNo { get; set; }
        public int BRSId { get; set; }
    }
}
