﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PMS.Web.ViewModels
{
    public class BRSModel
    {
        public int Id { get; set; }
        public string PISNo { get; set; }
        public string BRSNo { get; set; }
        public int BU { get; set; }
        public DateTime? BRSApprovedDate { get; set; }
        public string FilePath { get; set; }
        public string Filename { get; set; }
        public string Status { get; set; }
        public string Notes { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateCreated { get; set; }
        public string EditedBy { get; set; }
        public DateTime? DateEdited { get; set; }
        public bool Deleted { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DateDeleted { get; set; }

        public string Title { get; set; }
        public string AppName { get; set; }
        public DateTime? DateSubmitted { get; set; }
        public string ApprovedBy { get; set; }
        public string AppDevComment { get; set; }
        public string AppDevNote { get; set; }
        public DateTime? DateApproved { get; set; }
        public DateTime? CabReviewSchedule { get; set; }

        public PISInformation PISModel { get; set; }
    }
}