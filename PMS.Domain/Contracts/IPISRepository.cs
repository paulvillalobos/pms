﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PMS.Domain.Entities;

namespace PMS.Domain.Contracts
{
    public interface IPISRepository : IRepository<PIS>
    {
        PIS getPISDetails(Guid PkId,int Id);
        void UpdateIsBRS(PIS entity);
        void UpdateIsApproved(PIS entity);
        void UpdateIsSubmit(int Id);
        void UpdatePISRecommendation(PIS entity);
<<<<<<< HEAD
        void UpdateCabSchedule(PIS entity);
        void  UpdateApprovedPIS(int Id);
        void  UpdateSendBackPIS(PIS entity);
        //PIS getListStatus(string listname);
        IEnumerable<PIS> getListStatus(string listname);
=======
>>>>>>> e0c77c851bfb9fa3f68ef227cf7bc87ed6aca07c
    }
}
