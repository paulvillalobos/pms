﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PMS.Domain.Entities;

namespace PMS.Domain.Contracts
{
    public interface IBRSIncrementNo : IRepository<BRSEIncrementNo>
    {
        void UpdateBRSNum();
        BRSEIncrementNo GetBRSNo();
    }
}
