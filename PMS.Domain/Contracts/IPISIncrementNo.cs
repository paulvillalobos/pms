﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PMS.Domain.Entities;

namespace PMS.Domain.Contracts
{
    public interface IPISIncrementNo : IRepository<PISEIncrementNo>
    {
        void UpdatePISNum();
        PISEIncrementNo GetPISNo();
    }
}
