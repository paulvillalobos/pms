﻿using PMS.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Domain.Entities
{
    public class PISDev : EntityBase, IAggregateRoot
    {
        public virtual int Id { get; set; }
        public virtual string PISNo { get; set; }
        public virtual string DevName { get; set; }
        public virtual string Status { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual Nullable<System.DateTime> DateCreated { get; set; }
        public virtual string EditedBy { get; set; }
        public virtual Nullable<System.DateTime> DateEdited { get; set; }
        public virtual Nullable<bool> Deleted { get; set; }
        public virtual string DeletedBy { get; set; }
        public virtual Nullable<System.DateTime> DateDeleted { get; set; }

        public static PISDev Create(PISDev pISDev)
        {
            //Place your Business logic here
            pISDev.Id = pISDev.Id;
            return pISDev;
        }

        public static PISDev Update(PISDev pISDev)
        {
            //Place your Business logic here
            return pISDev;
        }

        public static PISDev Delete(PISDev pISDev)
        {
            //Place your Business logic here
            return pISDev;
        }
    }
}
