﻿using PMS.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Domain.Entities
{
    public class BRS : EntityBase, IAggregateRoot
    {
        // sample Create BRS
        public virtual int Id { get; set; }
        public virtual string PISNo { get; set; }
        public virtual string BRSNo { get; set; }
        public virtual int? BU { get; set; }
        public virtual Nullable<System.DateTime> BRSApprovedDate { get; set; }
        public virtual string FilePath { get; set; }
        public virtual string Filename { get; set; }
        public virtual string Status { get; set; }
        public virtual string Notes { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual Nullable<System.DateTime> DateCreated { get; set; }
        public virtual string EditedBy { get; set; }
        public virtual Nullable<System.DateTime> DateEdited { get; set; }
        public virtual Nullable<bool> Deleted { get; set; }
        public virtual string DeletedBy { get; set; }
        public virtual Nullable<System.DateTime> DateDeleted { get; set; }

        public virtual string Title { get; set; }
        public virtual string AppName { get; set; }
        public virtual Nullable<System.DateTime> DateSubmitted { get; set; }
        public virtual string ApprovedBy { get; set; }
        public virtual string AppDevComment { get; set; }
        public virtual string AppDevNote { get; set; }
        public virtual Nullable<System.DateTime> DateApproved { get; set; }
        public virtual Nullable<System.DateTime> CabReviewSchedule { get; set; }

        // Added by Rommel for PIS Record
        public virtual PIS pisDomain { get; set; }

        public static BRS Create(BRS bRS)
        {
            //Place your Business logic here
            bRS.Id = bRS.Id;
            return bRS;
        }

        public static BRS Update(BRS bRS)
        {
            //Place your Business logic here
            return bRS;
        }

        public static BRS Delete(BRS bRS)
        {
            //Place your Business logic here
            return bRS;
        }
    }
}
