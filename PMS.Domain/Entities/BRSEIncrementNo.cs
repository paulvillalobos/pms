﻿using PMS.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Domain.Entities
{
    public class BRSEIncrementNo : EntityBase, IAggregateRoot
    {
        public virtual int Id { get; set; }
        public virtual int BRSNo { get; set; }
        public virtual string BRSNodisplay { get; set; }

        public static BRSEIncrementNo Update(BRSEIncrementNo bRS)
        {
            //Place your Business logic here
            return bRS;
        }
    }
}
