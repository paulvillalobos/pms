﻿using PMS.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Domain.Entities
{
    public class PISEIncrementNo : EntityBase, IAggregateRoot
    {
        public virtual int Id { get; set; }
        public virtual int PISNo { get; set; }
        public virtual string PISNodisplay { get; set; }

        public static PISEIncrementNo Update(PISEIncrementNo pIS)
        {
            //Place your Business logic here
            return pIS;
        }
    }
}
