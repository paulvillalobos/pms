using PMS.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Domain.Entities
{
    public class PIS : EntityBase, IAggregateRoot
    {
        //public  Guid AggregateRootId { get; protected set; }
        public  Guid Pkid { get; set; }
        public virtual int Id { get; set; }
        public virtual string PISNo { get; set; }
        public virtual string Name { get; set; }
        public virtual string DepartmentBU { get; set; }
        public virtual string Position { get; set; }
        public virtual string Manager { get; set; }
        public virtual string BusinessObjective { get; set; }
        public virtual string Request { get; set; }
        public virtual string SolutionRecommendation { get; set; }
        public virtual Nullable<System.DateTime> DateReceived { get; set; }
        public virtual string ReceivedBy { get; set; }
        public virtual Nullable<System.DateTime> DateViewed { get; set; }
        public virtual string ReviewedBy { get; set; }
        public virtual string RequestStatus { get; set; }
        public virtual string ProjectWeight { get; set; }
        public virtual string AppDevTeam { get; set; }
        public virtual string AppManager { get; set; }
        public virtual string Title { get; set; }
        public virtual string AppName { get; set; }
        public virtual string AppType { get; set; }
        public virtual string AppRequestor { get; set; }
        public virtual string AppDevComment { get; set; }
        public virtual string AppDevNote { get; set; }
        public virtual Nullable<System.DateTime> DateSubmitted { get; set; }
        public virtual Nullable<System.DateTime> CabReviewSchedule { get; set; }
        public virtual string FilePath { get; set; }
        public virtual string FileName { get; set; }
        public virtual Nullable<bool> IsBRS { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual Nullable<System.DateTime> DateCreated { get; set; }
        public virtual string EditedBy { get; set; }
        public virtual Nullable<System.DateTime> DateEdited { get; set; }
        public virtual Nullable<bool> Deleted { get; set; }
        public virtual string DeletedBy { get; set; }
        public virtual Nullable<System.DateTime> DateDeleted { get; set; }
        public virtual Nullable<bool> IsDraft { get; set; }
        public virtual Nullable<bool> IsSubmit { get; set; }
        public virtual Nullable<bool> IsApproved { get; set; }
        public virtual string ApprovedBy { get; set; }
        public virtual Nullable<System.DateTime> DateApproved { get; set; }
<<<<<<< HEAD
        public virtual string AppDev { get; set; }
        public virtual Nullable<bool> IsApprovedPIS { get; set; }
        public virtual Nullable<System.DateTime> DateApprovedPIS { get; set; }
        public virtual string ApprovedPISBy { get; set; }
        public virtual Nullable<bool> IsSendBack { get; set; }
        public virtual Nullable<System.DateTime> DateSendBack { get; set; }
        public virtual string SendBackBy { get; set; }
        public virtual string SendBackNote { get; set; }
=======
        public string AppDev { get; set; }
>>>>>>> e0c77c851bfb9fa3f68ef227cf7bc87ed6aca07c

        public virtual string BRSNo { get; set; }
        public virtual int BRSId { get; set; }
        public static PIS Create(PIS pIS)
        {
            //Place your Business logic here
            pIS.Id = pIS.Id;
            return pIS;
        }

        public static PIS Update(PIS pIS)
        {
            //Place your Business logic here
            return pIS;
        }

        public static PIS Delete(PIS pIS)
        {
            //Place your Business logic here
            return pIS;
        }
    }
}
