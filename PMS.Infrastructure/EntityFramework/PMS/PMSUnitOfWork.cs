﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Infrastructure.EntityFramework.PMS
{
    public interface IPMSUnitOfWork : IDisposable
    {
        PMSEntities1 DbContext { get; }
        int Save();
    }
    public class PMSUnitOfWork : IPMSUnitOfWork
    {
        private PMSEntities1 context;

        //public UnitOfWork(string connectionString)
        //{
        //    this.ConnectionString = connectionString;
        //}

        public PMSEntities1 DbContext
        {
            get
            {
                if (context == null)
                {
                    context = new PMSEntities1();
                }
                return context;
            }
        }

        public int Save()
        {
            //return context.SaveChanges();
            try
            {
                return context.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }

        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (context != null)
                {
                    context.Dispose();
                    context = null;
                }
            }
        }
    }
}
