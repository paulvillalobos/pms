//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PMS.Infrastructure.EntityFramework.COMMON
{
    using System;
    using System.Collections.Generic;
    
    public partial class api_user
    {
        public int id { get; set; }
        public string user_name { get; set; }
        public string password { get; set; }
        public string authorize_role { get; set; }
        public string remarks { get; set; }
        public string created_by { get; set; }
        public System.DateTime date_created { get; set; }
        public string edited_by { get; set; }
        public Nullable<System.DateTime> date_edited { get; set; }
        public Nullable<bool> deleted { get; set; }
        public string deleted_by { get; set; }
        public Nullable<System.DateTime> date_deleted { get; set; }
    }
}
