﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Infrastructure.EntityFramework.COMMON
{
    public interface ICommonUnitOfWork : IDisposable
    {
        COMMON_DBEntities DbContext { get; }
        int Save();
    }

    public class CommonUnitOfWork : ICommonUnitOfWork
    {
        private COMMON_DBEntities context;

        //public UnitOfWork(string connectionString)
        //{
        //    this.ConnectionString = connectionString;
        //}

        public COMMON_DBEntities DbContext
        {
            get
            {
                if (context == null)
                {
                    context = new COMMON_DBEntities();
                }
                return context;
            }
        }

        public int Save()
        {
            return context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (context != null)
                {
                    context.Dispose();
                    context = null;
                }
            }
        }
    }
}
