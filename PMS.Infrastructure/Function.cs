﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Infrastructure
{
    public class Function
    {
        public static string PopulateGeneratedNumber(string accountNumber)
        {
            string a = "000000000";
            string b = "00000000";
            string c = "0000000";
            string d = "000000";
            string e = "00000";
            string f = "0000";
            string g = "000";
            string h = "00";
            string i = "0";

            //9827915737
            UInt64 CLkey = Convert.ToUInt64(accountNumber);
            //int CLkey = _StrClkey;

            if (CLkey <= 9) { string CLkey3 = Convert.ToString(a) + CLkey; accountNumber = CLkey3; }
            if (CLkey > 9 && CLkey <= 99) { string CLkey3 = Convert.ToString(b) + CLkey; accountNumber = CLkey3; }
            if (CLkey > 99 && CLkey <= 999) { string CLkey3 = Convert.ToString(c) + CLkey; accountNumber = CLkey3; }
            if (CLkey > 999 && CLkey <= 9999) { string CLkey3 = Convert.ToString(d) + CLkey; accountNumber = CLkey3; }
            if (CLkey > 9999 && CLkey <= 99999) { string CLkey3 = Convert.ToString(e) + CLkey; accountNumber = CLkey3; }
            if (CLkey > 99999 && CLkey <= 999999) { string CLkey3 = Convert.ToString(f) + CLkey; accountNumber = CLkey3; }
            if (CLkey > 999999 && CLkey <= 9999999) { string CLkey3 = Convert.ToString(g) + CLkey; accountNumber = CLkey3; }
            if (CLkey > 9999999 && CLkey <= 99999999) { string CLkey3 = Convert.ToString(h) + CLkey; accountNumber = CLkey3; }
            if (CLkey > 99999999 && CLkey <= 999999999) { string CLkey3 = Convert.ToString(i) + CLkey; accountNumber = CLkey3; }
            if (CLkey > 999999999) { string CLkey3 = "" + CLkey; accountNumber = CLkey3; }

            return accountNumber;
        }
    }
}
