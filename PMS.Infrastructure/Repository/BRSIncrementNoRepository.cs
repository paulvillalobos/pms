﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

using PMS.Domain.Entities;
using PMS.Infrastructure.EntityFramework.PMS;
using DomainEntity = PMS.Domain.Entities;
using PMS.Domain.Contracts;
using PMS.Infrastructure;


namespace PMS.Infrastructure.Repository
{
    public class BRSIncrementNoRepository : IBRSIncrementNo
    {
        private IMapper _mapper;

        public BRSIncrementNoRepository(IMapper mapper)
        {
            if (mapper == null) throw new ArgumentNullException("Mapper");
            _mapper = mapper;
        }

        public IEnumerable<DomainEntity.BRSEIncrementNo> GetAll()
        {
            throw new NotImplementedException();
        }

        public DomainEntity.BRSEIncrementNo GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DomainEntity.BRSEIncrementNo> Find(Expression<Func<DomainEntity.BRSEIncrementNo, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public void Add(DomainEntity.BRSEIncrementNo entity)
        {
            throw new NotImplementedException();
        }

        public void Update(DomainEntity.BRSEIncrementNo entity)
        {
            throw new NotImplementedException();
        }

        public void UpdateBRSNum()
        {
            using (var context = new PMSEntities1())
            {
                BRSIncrementNo brs = context.BRSIncrementNoes.FirstOrDefault();
                brs.BRSNo = brs.BRSNo + 1;           
                context.SaveChanges();
            }
        }

        public DomainEntity.BRSEIncrementNo GetBRSNo()
        {
            using (var context = new PMSEntities1())
            {
                BRSIncrementNo brsNo = context.BRSIncrementNoes.FirstOrDefault();
                return new DomainEntity.BRSEIncrementNo
                {
                    Id = brsNo.Id,
                    BRSNodisplay = Function.PopulateGeneratedNumber(Convert.ToString(brsNo.BRSNo + 1)),
                };
            }
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
