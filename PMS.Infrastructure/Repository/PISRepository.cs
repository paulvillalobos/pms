﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

using PMS.Domain.Entities;
using PMS.Infrastructure.EntityFramework.PMS;
using DomainEntity = PMS.Domain.Entities;
using PMS.Domain.Contracts;

namespace PMS.Infrastructure.Repository
{
    public class PISRepository : IPISRepository
    {
        private IMapper _mapper;

        public PISRepository(IMapper mapper)
        {
            if (mapper == null) throw new ArgumentNullException("Mapper");
            _mapper = mapper;
        }

        public void Add(DomainEntity.PIS entity)
        {
            try
            {
                using (var context = new PMSEntities1())
                {
                    context.PIS.Add(_mapper.Map<DomainEntity.PIS, PI>(entity));
                    context.SaveChanges();
                }

            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }

        }


        public void Update(DomainEntity.PIS entity)
        {
            using (var context = new PMSEntities1())
            {
                PI pis = context.PIS.Where(e => e.Id == entity.Id).FirstOrDefault();
                pis.Name = entity.Name;
                pis.Manager = entity.Manager;
                pis.DepartmentBU = entity.DepartmentBU;
                pis.Position = entity.Position;
                pis.BusinessObjective = entity.BusinessObjective;
                pis.Request = entity.Request;
                pis.DateEdited = entity.CreatedDate;
                pis.IsDraft = entity.IsDraft;
                pis.IsSubmit = entity.IsSubmit;
                context.SaveChanges();
            }
        }


        public void UpdateIsBRS(DomainEntity.PIS entity)
        {
            using (var context = new PMSEntities1())
            {
                PI pis = context.PIS.Where(e => e.PISNo == entity.PISNo).FirstOrDefault();
                pis.IsBRS = true;
                pis.RequestStatus = "R";
                context.SaveChanges();
            }
        }


        //Added by Sir Rommel
        public void UpdateIsApproved(DomainEntity.PIS entity)
        {
            using (var context = new PMSEntities1())
            {
                PI pis = context.PIS.Where(e => e.PISNo == entity.PISNo).FirstOrDefault();
                pis.IsApproved = true;
                pis.AppName = entity.AppName;
                pis.Title = entity.Title;
                pis.AppDevNote = entity.AppDevNote;
                pis.AppDevComment = entity.AppDevComment;
                pis.ApprovedBy = entity.ApprovedBy;
                pis.DateApproved = entity.DateApproved;
                pis.RequestStatus = "B";
                context.SaveChanges();
            }
        }



        public void UpdateIsSubmit(int Id)
        {
            using (var context = new PMSEntities1())
            {
                PI pis = context.PIS.Where(e => e.Id == Id).FirstOrDefault();
                pis.IsSubmit = true;
                pis.IsDraft = false;
                pis.DateSubmitted = DateTime.Now;
                pis.RequestStatus = "S";
                context.SaveChanges();
            }
        }

        public DomainEntity.PIS getPISDetails(Guid PkId, int Id)
        {
            using (var context = new PMSEntities1())
            {
                PI pis;
                if (Id != 0)
                {
                    pis = context.PIS.Where(e => e.Id == Id).FirstOrDefault();
                }
                else
                {
                    pis = context.PIS.Where(e => e.PkId == PkId).FirstOrDefault();
                }

                return new DomainEntity.PIS
                {
                    Name = pis.Name,
                    Manager = pis.Manager,
                    DepartmentBU = pis.DepartmentBU,
                    Position = pis.Position,
                    BusinessObjective = pis.BusinessObjective,
                    Request = pis.Request,
                    Pkid = pis.PkId,
                    PISNo = pis.PISNo,
                    Id = pis.Id,
                    IsDraft = pis.IsDraft,
                    IsSubmit = pis.IsSubmit,
<<<<<<< HEAD
                    IsApprovedPIS = pis.IsApprovedPIS,
                    IsBRS = pis.IsBRS,
                    IsSendBack = pis.IsSendBack,
                    IsApproved = pis.IsApproved,
=======
>>>>>>> e0c77c851bfb9fa3f68ef227cf7bc87ed6aca07c
                    AppType = pis.AppType,
                    ProjectWeight = pis.ProjectWeight,
                    AppDevTeam = pis.AppDevTeam,
                    AppManager = pis.AppManager,
                    AppName = pis.AppName,
                    AppDev = pis.AppDev,
                    SolutionRecommendation = pis.SolutionRecommendation,
<<<<<<< HEAD
                    CabReviewSchedule = pis.CabReviewSchedule,
                    DateApprovedPIS = pis.DateApprovedPIS,
                    ApprovedPISBy = pis.ApprovedPISBy,
                    SendBackNote = pis.SendBackNote,

=======
>>>>>>> e0c77c851bfb9fa3f68ef227cf7bc87ed6aca07c
                };
            }
        }


        public IEnumerable<DomainEntity.PIS> GetAll()
        {
            using (var context = new PMSEntities1())
            {
      
               IQueryable <DomainEntity.PIS> pisList =
                    context.PIS
                    //.Join(context.BRS, pis => pis.PISNo, brs => brs.PISNo, (P, B) => new { pis = P, brs = B })
                    .Select(e => new DomainEntity.PIS
                    {
                        Pkid = e.PkId,
                        Id = e.Id,
                        PISNo = e.PISNo,
                        Title = e.Title,
                        AppName = e.AppName,
                        Name = e.Name,
                        DateCreated = e.DateCreated,
                        IsDraft = e.IsDraft,
                        IsSubmit = e.IsSubmit,
<<<<<<< HEAD
                        IsApprovedPIS = e.IsApprovedPIS,
                        IsBRS = e.IsBRS,
                        IsSendBack = e.IsSendBack,
                        IsApproved = e.IsApproved,
                        CabReviewSchedule = e.CabReviewSchedule,
                        AppType = e.AppType,
                        BRSId = e.Id,
                    });
                //.Where(e => e.EmployeeId.Contains("TESTCORE") && e.EmployeeId.Contains("TESTCORE"));
                //.Take(100);
=======
                    })
                    //.Where(e => e.EmployeeId.Contains("TESTCORE"))
                    .Take(100);
>>>>>>> e0c77c851bfb9fa3f68ef227cf7bc87ed6aca07c
                return pisList.ToList();
            }
        }


        public void Remove(int id)
        {
            using (var context = new PMSEntities1())
            {
                PI pis = context.PIS.Single(e => e.Id == id);
                context.PIS.Remove(pis);
                context.SaveChanges();
            }
        }


        public void UpdatePISRecommendation(DomainEntity.PIS entity)
        {
            using (var context = new PMSEntities1())
            {
                PI pis = context.PIS.Where(e => e.Id == entity.Id).FirstOrDefault();
                pis.AppType = entity.AppType;
                pis.ProjectWeight = entity.ProjectWeight;
                pis.AppDevTeam = entity.AppDevTeam;
                pis.AppManager = entity.AppManager;
                pis.AppDev = entity.AppDev;
                pis.SolutionRecommendation = entity.SolutionRecommendation;
                context.SaveChanges();
            }
        }


<<<<<<< HEAD
        public void UpdateCabSchedule(DomainEntity.PIS entity)
        {
            using (var context = new PMSEntities1())
            {
                PI pis = context.PIS.Where(e => e.Id == entity.Id).FirstOrDefault();
                pis.CabReviewSchedule = entity.CabReviewSchedule;
                pis.RequestStatus = entity.RequestStatus;
                context.SaveChanges();
            }
        }



        public void UpdateApprovedPIS(int Id)
        {
            using (var context = new PMSEntities1())
            {
                PI pis = context.PIS.Where(e => e.Id == Id).FirstOrDefault();
                pis.IsApprovedPIS = true;
                pis.ApprovedPISBy = "admin";
                pis.DateApprovedPIS = DateTime.Now;
                pis.RequestStatus = "A";
                context.SaveChanges();
            }

        }


        public void UpdateSendBackPIS(DomainEntity.PIS entity)
        {
            using (var context = new PMSEntities1())
            {
                PI pis = context.PIS.Where(e => e.Id == entity.Id).FirstOrDefault();
                pis.DateSendBack = entity.DateSendBack;
                pis.IsSendBack = entity.IsSendBack;
                pis.SendBackBy = entity.SendBackBy;
                pis.SendBackNote = entity.SendBackNote;
                pis.RequestStatus = entity.RequestStatus;
                context.SaveChanges();
            }
        }




        public IEnumerable<DomainEntity.PIS> getListStatus(string listname)
        {
            using (var context = new PMSEntities1())
            {
                if (listname == "cab")
                {

                    IQueryable<DomainEntity.PIS> pisList = context.PIS
                   .Select(e => new DomainEntity.PIS
                   {
                       Pkid = e.PkId,
                       Id = e.Id,
                       PISNo = e.PISNo,
                       Title = e.Title,
                       AppName = e.AppName,
                       Name = e.Name,
                       DateCreated = e.DateCreated,
                       IsDraft = e.IsDraft,
                       IsSubmit = e.IsSubmit,
                       IsApprovedPIS = e.IsApprovedPIS,
                       IsBRS = e.IsBRS,
                       IsSendBack = e.IsSendBack,
                       IsApproved = e.IsApproved,
                       CabReviewSchedule = e.CabReviewSchedule,
                       AppType = e.AppType,
                   })
                   .Where(e => e.CabReviewSchedule != null && e.IsApprovedPIS == null && e.IsDraft == false && e.IsSubmit == true);
                    //.Take(100);
                    return pisList.ToList();
                }

                else if (listname == "pis")
                {
                    IQueryable<DomainEntity.PIS> pisList = context.PIS
                                     .Select(e => new DomainEntity.PIS
                                     {
                                         Pkid = e.PkId,
                                         Id = e.Id,
                                         PISNo = e.PISNo,
                                         Title = e.Title,
                                         AppName = e.AppName,
                                         Name = e.Name,
                                         DateCreated = e.DateCreated,
                                         IsDraft = e.IsDraft,
                                         IsSubmit = e.IsSubmit,
                                         IsApprovedPIS = e.IsApprovedPIS,
                                         IsBRS = e.IsBRS,
                                         IsSendBack = e.IsSendBack,
                                         IsApproved = e.IsApproved,
                                         CabReviewSchedule = e.CabReviewSchedule,
                                         AppType = e.AppType,
                                     })
                                     .Where(e => e.IsApprovedPIS == true);
                    //.Take(100);
                    return pisList.ToList();
                }

                else if (listname == "new")
                {
                    IQueryable<DomainEntity.PIS> pisList = context.PIS
                                     .Select(e => new DomainEntity.PIS
                                     {
                                         Pkid = e.PkId,
                                         Id = e.Id,
                                         PISNo = e.PISNo,
                                         Title = e.Title,
                                         AppName = e.AppName,
                                         Name = e.Name,
                                         DateCreated = e.DateCreated,
                                         IsDraft = e.IsDraft,
                                         IsSubmit = e.IsSubmit,
                                         IsApprovedPIS = e.IsApprovedPIS,
                                         IsBRS = e.IsBRS,
                                         IsSendBack = e.IsSendBack,
                                         IsApproved = e.IsApproved,
                                         CabReviewSchedule = e.CabReviewSchedule,
                                         AppType = e.AppType,
                                     })
                                     .Where(e => e.IsDraft == false && e.IsSubmit == true && e.CabReviewSchedule == null);
                    //.Take(100);
                    return pisList.ToList();
                }

                else
                {
                    IQueryable<DomainEntity.PIS> pisList = context.PIS
                                                        .Join(context.BRS, pis => pis.PISNo, brs => brs.PISNo, (P, B) => new { pis = P, brs = B })
                                                        .Select(e => new DomainEntity.PIS
                                                        {
                                                            Pkid = e.pis.PkId,
                                                            Id = e.pis.Id,
                                                            PISNo = e.pis.PISNo,
                                                            Title = e.pis.Title,
                                                            AppName = e.pis.AppName,
                                                            Name = e.pis.Name,
                                                            DateCreated = e.pis.DateCreated,
                                                            IsDraft = e.pis.IsDraft,
                                                            IsSubmit = e.pis.IsSubmit,
                                                            IsApprovedPIS = e.pis.IsApprovedPIS,
                                                            IsBRS = e.pis.IsBRS,
                                                            IsSendBack = e.pis.IsSendBack,
                                                            IsApproved = e.pis.IsApproved,
                                                            CabReviewSchedule = e.pis.CabReviewSchedule,
                                                            AppType = e.pis.AppType,
                                                            AppDev = e.pis.AppDev,
                                                            AppManager = e.pis.AppManager,
                                                            BRSNo = e.brs.BRSNo,
                                                            BRSId = e.brs.Id,
                                                        });
                    //.Where(e => e.IsDraft == false && e.IsSubmit == true && e.CabReviewSchedule == null);
                    //.Take(100);

                    return pisList.ToList();
                }
            }
        }


=======
>>>>>>> e0c77c851bfb9fa3f68ef227cf7bc87ed6aca07c
        public DomainEntity.PIS GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DomainEntity.PIS> Find(Expression<Func<DomainEntity.PIS, bool>> predicate)
        {
            throw new NotImplementedException();
        }

    }
}
