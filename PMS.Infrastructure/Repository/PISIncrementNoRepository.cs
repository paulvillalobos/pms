﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

using PMS.Domain.Entities;
using PMS.Infrastructure.EntityFramework.PMS;
using DomainEntity = PMS.Domain.Entities;
using PMS.Domain.Contracts;


namespace PMS.Infrastructure.Repository
{
    public class PISIncrementNoRepository : IPISIncrementNo
    {
        private IMapper _mapper;

        public PISIncrementNoRepository(IMapper mapper)
        {
            if (mapper == null) throw new ArgumentNullException("Mapper");
            _mapper = mapper;
        }

        public IEnumerable<DomainEntity.PISEIncrementNo> GetAll()
        {
            throw new NotImplementedException();
        }

        public DomainEntity.PISEIncrementNo GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DomainEntity.PISEIncrementNo> Find(Expression<Func<DomainEntity.PISEIncrementNo, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public void Add(DomainEntity.PISEIncrementNo entity)
        {
            throw new NotImplementedException();
        }

        public void Update(DomainEntity.PISEIncrementNo entity)
        {
            throw new NotImplementedException();
        }

        public void UpdatePISNum()
        {
            using (var context = new PMSEntities1())
            {
                PISIncrementNo brs = context.PISIncrementNoes.FirstOrDefault();
                brs.PISNo = brs.PISNo + 1;

                context.SaveChanges();
            }
        }

        public DomainEntity.PISEIncrementNo GetPISNo()
        {
            using (var context = new PMSEntities1())
            {
                PISIncrementNo pisNo = context.PISIncrementNoes.FirstOrDefault();
                return new DomainEntity.PISEIncrementNo
                {
                    Id = pisNo.Id,
                    PISNodisplay = Function.PopulateGeneratedNumber(Convert.ToString(pisNo.PISNo + 1)),
                };
            }
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
