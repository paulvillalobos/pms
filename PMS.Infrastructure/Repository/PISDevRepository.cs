﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

using PMS.Domain.Entities;
using PMS.Infrastructure.EntityFramework.PMS;
using DomainEntity = PMS.Domain.Entities;
using PMS.Domain.Contracts;

namespace PMS.Infrastructure.Repository
{
    public class PISDevRepository : IPISDevRepository
    {
        private IMapper _mapper;

        public PISDevRepository(IMapper mapper)
        {
            if (mapper == null) throw new ArgumentNullException("Mapper");
            _mapper = mapper;
        }

        public IEnumerable<DomainEntity.PISDev> GetAll()
        {
            throw new NotImplementedException();
        }

        public DomainEntity.PISDev GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DomainEntity.PISDev> Find(Expression<Func<DomainEntity.PISDev, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public void Add(DomainEntity.PISDev entity)
        {
            throw new NotImplementedException();
        }

        public void Update(DomainEntity.PISDev entity)
        {
            throw new NotImplementedException();
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
