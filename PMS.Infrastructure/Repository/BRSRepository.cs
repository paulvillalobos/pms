﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

using PMS.Domain.Entities;
using PMS.Infrastructure.EntityFramework.PMS;
using DomainEntity = PMS.Domain.Entities;
using PMS.Domain.Contracts;

namespace PMS.Infrastructure.Repository
{
    public class BRSRepository : IBRSRepository
    {
        private IMapper _mapper;

        public BRSRepository(IMapper mapper)
        {
            if (mapper == null) throw new ArgumentNullException("Mapper");
            _mapper = mapper;
        }

        public IEnumerable<DomainEntity.BRS> GetAll()
        {
            using (var context = new PMSEntities1())
            {
                IQueryable<DomainEntity.BRS> pisList = context.BRS
                    .Join(context.PIS, brs => brs.PISNo, pis => pis.PISNo, (B, P) => new { bRS = B, pIS = P })
                    .Select(e => new DomainEntity.BRS
                    {
                        Id = e.bRS.Id,
                        BRSNo = e.bRS.BRSNo,
                        PISNo = e.bRS.PISNo,
                        BRSApprovedDate = e.bRS.BRSApprovedDate,
                        Filename = e.bRS.Filename,
                        FilePath = e.bRS.FilePath,
                        DateCreated = e.bRS.DateCreated,

                        Title = e.pIS.Title,
                        AppName = e.pIS.AppName,
                        DateSubmitted = e.pIS.DateSubmitted,
                        ApprovedBy = e.pIS.ApprovedBy,
                        CabReviewSchedule = e.pIS.CabReviewSchedule
                    })
                    //.Where(e => e.EmployeeId.Contains("TESTCORE"))
                    .Take(100);
                return pisList.ToList();
            }
        }

        public DomainEntity.BRS GetById(int id)
        {
            using (var context = new PMSEntities1())
            {
                DomainEntity.BRS brs = context.BRS
                    .Join(context.PIS, b => b.PISNo, pis => pis.PISNo, (B, P) => new { bRS = B, pIS = P })
                    .Where(e => e.bRS.Id == id)
                    .Select(r => new DomainEntity.BRS
                     {
                        Id = r.bRS.Id,
                        PISNo = r.bRS.PISNo,
                        BRSNo = r.bRS.BRSNo,
                        BU = r.bRS.BU,
                        BRSApprovedDate = r.bRS.BRSApprovedDate,
                        Filename = r.bRS.Filename,
                        FilePath = r.bRS.FilePath,
                        Notes = r.bRS.Notes,
                        CreatedBy = r.bRS.CreatedBy,
                        DateCreated = r.bRS.DateCreated,
                        EditedBy = r.bRS.EditedBy,
                        DateEdited = r.bRS.DateEdited,
                        DeletedBy = r.bRS.DeletedBy,
                        DateDeleted = r.bRS.DateDeleted,

                        Title = r.pIS.Title,
                        AppName = r.pIS.AppName,
                        ApprovedBy = r.pIS.ApprovedBy,
                        AppDevComment = r.pIS.AppDevComment,
                        AppDevNote = r.pIS.AppDevNote,
                        DateApproved = r.pIS.DateApproved

                    }).FirstOrDefault();

                    return brs;
                //return new DomainEntity.BRS
                //{
                //    Id = brs.Id,
                //    PISNo = brs.PISNo,
                //    BRSNo = brs.BRSNo,
                //    BU = brs.BU,
                //    BRSApprovedDate = brs.BRSApprovedDate,
                //    Filename = brs.Filename,
                //    FilePath = brs.FilePath,
                //    Notes = brs.Notes,
                //    CreatedBy = brs.CreatedBy,
                //    DateCreated = brs.DateCreated,
                //    EditedBy = brs.EditedBy,
                //    DateEdited = brs.DateEdited,
                //    DeletedBy = brs.DeletedBy,
                //    DateDeleted = brs.DateDeleted

                //};
            }
        }

        public IEnumerable<DomainEntity.BRS> Find(Expression<Func<DomainEntity.BRS, bool>> predicate)
        {
            using (var context = new PMSEntities1())
            {
                return context.BRS
                .Where(Helpers.ConvertPredicate<DomainEntity.BRS, BR>(predicate))
                .Select(r => new DomainEntity.BRS
                {
                    Id = r.Id,
                    BRSNo = r.BRSNo,
                    BRSApprovedDate = r.BRSApprovedDate,
                    Filename = r.Filename,
                    FilePath = r.FilePath,
                    CreatedBy = r.CreatedBy,
                    DateCreated = r.DateCreated,
                    EditedBy = r.EditedBy,
                    DateEdited = r.DateEdited,
                    DeletedBy = r.DeletedBy,
                    DateDeleted = r.DateDeleted
                });
            }

        }

        public void Add(DomainEntity.BRS entity)
        {
            try
            {
                using (var context = new PMSEntities1())
                {
                    context.BRS.Add(_mapper.Map<DomainEntity.BRS, BR>(entity));
                    context.SaveChanges();
                }

            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }

        public void Update(DomainEntity.BRS entity)
        {
            using (var context = new PMSEntities1())
            {
                BR brs = context.BRS.Where(e => e.BRSNo == entity.BRSNo).FirstOrDefault();
                brs.BRSApprovedDate = entity.BRSApprovedDate;
                //brs.Filename = entity.Filename;
                //brs.FilePath = entity.FilePath;
                brs.EditedBy = entity.EditedBy;
                brs.DateEdited = entity.DateEdited;
                context.SaveChanges();
            }
        }

        public void UpdatePISNo(DomainEntity.BRS entity)
        {
            using (var context = new PMSEntities1())
            {
                BR brs = context.BRS.Where(e => e.PISNo == entity.PISNo).FirstOrDefault();
                brs.Filename = entity.Filename;
                brs.FilePath = entity.FilePath;
                context.SaveChanges();
            }
        }

        public void Remove(int id)
        {
            using (var context = new PMSEntities1())
            {
                BR brs = context.BRS.Single(e => e.Id == id);
                context.BRS.Remove(brs);
                context.SaveChanges();
            }
        }
    }
}
